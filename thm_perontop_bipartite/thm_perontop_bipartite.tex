\input{../top_preamble}

We \rrh{have identified} \(S_n\) with \rrh{the subset of \(\C S_n\) consisting of} the indicator functions of the singleton \rrh{subsets of \(S_n\)} in the natural way. So in \(d_\sigma(A)\), the \(\sigma\) is the function that sends \(\sigma\) to 1 and every other element of \(S_n\) to 0 and \(d_\sigma(A) = \prod_{i=1}^n a_{i\sigma(i)}\). Given any \(c \in \C S_n\), we can then write
\[
d_c(A) = \sum_{\sigma \in S_n} c(\sigma) \prod_{i=1}^n a_{i\sigma(i)} = \sum_{\sigma \in S_n} c(\sigma) d_\sigma(A).
\]
\begin{lemma}\label{lemma_perontop_bipartite}
Let \(A\in\mathbb{C}^{n\times n}\) be \rrh{a} positive semidefinite matrix such that \(\mathrm{d}_\sigma(A)\geq 0\) for every \(\sigma\in S_n\). Then if \(H \leq S_n\) and \(\chi \in \Irr(H)\) we have \(\per(A)\geq \bar{d}_\chi(A)\).
\end{lemma}%
\begin{proof}%
Let \(A\in\mathbb{C}^{n\times n}\) be positive semidefinite and assume that \(\mathrm{d}_\sigma(A)\geq 0\) for every \(\sigma\in S_n\). \ab{Let \(H \leq S_n\) and let \(\chi \in \Irr(H)\).} We know that for each \(\sigma\in H\), \(\left|\frac{\chi(\sigma)}{\chi(e)}\right| \leq 1\) because \(\chi(\sigma)\) is a sum of \(\chi(e)\)-many roots of 1 \cite[8.5(2) and proof, p.~18]{rrhreps}, so \(|\chi(\sigma)|\) cannot be greater than \(\chi(e)\).

We have
\begin{align*}
\bar{d}_\chi(A)%
&=\displaystyle\frac{1}{\chi(e)}%
\sum_{\sigma\in\mathrm{H}}%
\chi(\sigma)d_\sigma(A)\\
&=\displaystyle\sum_{\sigma\in\mathrm{H}}%
\frac{\chi(\sigma)}{\chi(e)}%
d_\sigma(A)\\
&\leq\left|\sum_{\sigma\in\mathrm{H}}%
\frac{\chi(\sigma)}{\chi(e)}%
d_\sigma(A)\right|\\
&\leq\sum_{\sigma\in\mathrm{H}}%
\left|\frac{\chi(\sigma)}{\chi(e)}\right|%
d_\sigma(A)\\
&\leq\displaystyle\sum_{\sigma\in\mathrm{H}}%
d_\sigma(A)\\
& \leq\displaystyle\sum_{\sigma\in\mathrm{S}_n}%
d_\sigma(A)\\
&=\mathrm{per}(A).%
\end{align*}%
\end{proof}%
\begin{theorem}
Let \(H \leq S_n\) and \(\chi \in \Irr(H)\). Let \(B\) be the set of laplacian matrices of bipartite graphs with \(n\) vertices. Then
\[\chi \leq_B 1.\]
\end{theorem}
\begin{proof}
We will show that every element of \(B\) satisfies the hypothesis of Lemma \ref{lemma_perontop_bipartite}. Suppose \(G\) is a bipartite graph with vertices \(\{v_1,\dots,v_n\}\) and put \(A=(a_{ij})=L(G)\). Let \(\sigma \in S_n\). We denote by unfix(\(\sigma\)) the set of elements of \(\{v_1,\dots,v_n\}\) that \(\sigma\) does not leave constant (with the action \(\sigma(v_i)=v_{\sigma(i)})\). \\
Then
\[
d_\sigma(A) = \prod_{i \in \{1,\dots,n\}}a_{i,\sigma(i)} \neq 0\quad \Leftrightarrow\quad\forall v \in \mathrm{unfix}(\sigma), \textrm{ \(v\) is adjacent to \(\sigma(v)\).}
\]
Suppose \(d_\sigma(A) \neq 0\), and choose \(v \in \mathrm{unfix}(\sigma)\). Let \(J\) be the orbit of \(v\) under \(\langle\sigma\rangle\). Either the elements of \(J\) form a cycle in \(G\) or \(J\) is a pair of adjacent vertices. Since \(G\) is bipartite, every cycle of \(G\) has an even number of elements. Thus \(|J|\) is even. Let S be the set of orbits of unfix(\(\sigma\)). Then we have
\begin{align*}
d_\sigma(A) %
&=\prod_{i \in \{1,\dots,n\}}%
a_{i,\sigma(i)}\\
&=\left[\prod_{v_i \in \mathrm{fix}(\sigma)}a_{i,i}\right]%
\prod_{v_i \in \mathrm{unfix}(\sigma)}%
a_{i,\sigma(i)}\\
&=\left[\prod_{v_i \in \mathrm{fix}(\sigma)}a_{i,i}\right]%
\prod_{\mathrm{J} \in \mathrm{S}}%
\underbrace{\prod_{v_i\in\mathrm{J}}%
a_{i,\sigma(i)}}_{(-1)^{|J|}=1}.
\end{align*}
%\abnote{If I label the vertices of G using the integers \(\{1,\dots,n\}\) instead of \(\{v_1,\dots,v_n\}\) I can cut down on the subscript depth. Would that be better?}
%\rrhnote{I can see the advantages of that here.  For instance, one really needs to define \(\operatorname{unfix}(\sigma)\) to be the set of \(i\) for which \(\sigma(i)\not=i\).  This would make the indexing in your products nicer.  Later proofs might work better using \(v_i\), though, so it might be too early to decide.}

Since the diagonal entries of \(L(G)\) are just the degrees of the corresponding vertices, the product of the diagonal is positive. So \(d_\sigma(A) > 0\). This happens for every \(\sigma\in H\) for which \(d_\sigma(A)\) is nonzero. The conclusion follows from Lemma \ref{lemma_perontop_bipartite}.
\end{proof}


\input{../top_postamble}
