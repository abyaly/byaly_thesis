\input{../top_preamble.tex}

\chapter{Introduction}
Let \(H\) be a set. Then \(\C H\) is the set of functions from \(H\) to \(\C\). This forms a vector space using pointwise addition and the usual scalar multiplication. There is a natural embedding \(\iota_H : H \rightarrow \C H\) such that \(\iota_H(\sigma)\) is the indicator function on \(\{\sigma\}\). When it is clear from context we will use the same symbol to refer to \(\sigma \in H\) and the corresponding element \(\iota_H(\sigma) \in \C H\). The image \(\iota_H(H)\) forms a basis for \(\C H\). 

If \(G\) is a group then the group multiplication on \(\iota_G(G)\) %
uniquely extends to a bilinear map \(* : \C G \times \C G \rightarrow \C G\). The pair \((\C G, *)\) is called the \textbf{group algebra} of \(G\) over \(\C\).

Given two functions \(f\) and \(g\) with codomain \(\C\), we call \(f\) and \(g\) \textbf{equivalent} if they agree on the intersection of their domains and they are both zero outside the intersection.

Let \(f\) be a partial function from the symmetric group \labelx{notation:Sn}\(S_n\) to \(\C\) (a function with codomain \(\C\) and with domain any subset of \(S_n\)).\\
Define the \textbf{generalized matrix function} \(d_f :\, \C^{n \times n} \rightarrow \C\) corresponding to \(f\)
by \[d_f(A)=\sum_{\sigma \in \text{dom } f}f(\sigma)\prod_{i =1}^na_{i, \sigma(i)},\]
where \(A = (a_{ij}) \in \C^{n\times n}\). \\

Note that if \(f\) and \(g\) are equivalent partial functions from \(S_n\) to \(\C\), then \(d_f = d_g\). Since this dissertation is primarily about these generalized matrix functions, it \ab{will not} be necessary to distinguish between equivalent partial functions. From here on we will consider \labelx{notation:CG} \(\C G\) to be a set of equivalence classes. Given an equivalence class \(f\) in \(\C S_n\), \labelx{notation:dc}\(d_f\) is the generalized matrix function corresponding to any function in the class.

Let \(H\) be a subset of \(G\). Then \(\C H\) is a set of equivalence classes to which functions from \(H\) to \(\C\) belong. But every function \(f\) from \(H\) to \(\C\) can be extended to an equivalent function \(\bar{f} : G \rightarrow \C\) by setting \(\bar{f}(x)=0\) for \(x \in G\backslash H\).  So \(\C H\) is a subset of \(\C G\).

\labelx{notation:leqS}Given \(S \subseteq \C^{n\times n}\), we partially order \(\C S_n\) by putting \[ f \preceq_S g \quad \Leftrightarrow \quad d_f(A) \leq d_g(A) \,\, \forall A \in S.\]
\labelx{notation:dcbar}If \(f \in \C S_n\) and \(f(\e) > 0\), %
we define \(\bar{d}_f(A) = \frac{d_f(A)}{f(\e)}\). We partially order \(\C S_n\) a second way by putting
\[ f \leq_S g \quad \Leftrightarrow \quad %
g(\e)d_f(A) \leq f(\e)d_g(A)%
\,\, \forall A \in S,\]
which, when \(\bar{d}_f\) and \(\bar{d}_g\) are both defined, is equivalent to
\[ f \leq_S g \quad \Leftrightarrow \quad %
\bar{d}_f(A) \leq \bar{d}_g(A)%
 \,\, \forall A \in S.\]%
We may think of \(\leq_S\) as the analogue of \(\preceq_S\) on the projective space \((\C S_n \backslash \{0\}) / \approx\), where \(v\approx w\) if and only if \(v\) and \(w\) are parallel.
\begin{theorem}
Let \(f,g \in \C S_n\) such that \(f(\e) > 0\) and \(g(\e) > 0\). Then %
\[ f \leq_S g %
\quad \Leftrightarrow \quad%
\frac{f}{f(\e)} \preceq_S \frac{g}{g(\e)}\]
\end{theorem}
\begin{proof}
This follows by the linearity of \(f \mapsto d_f\).
\end{proof}
When \(f \leq g\) is written with the subscript omitted it will mean \(f\leq_{\mathcal H} g\), where \(\mathcal H\)  is the set of positive semidefinite matrices in \(\C^{n \times n}\). Similarly, \(f \preceq g\) means \(f \preceq_{\mathcal H} g\).

\begin{remark}%
\label{remark:realspan}%
If \(f,g \in \C S_n\) and \(S \subseteq \C ^ {n \times n}\), the statement \(f \preceq_S g\) is also an assertion that \(d_f(A)\) and \(d_g(A)\) are real for every \(A \in S\). Furthermore, if \(S\) is nonempty the set \(\{ x \in \C S_n \,:\, \exists y \in \C S_n ( x \preceq_S y) \}\) is not closed under scalar multplication unless we restrict ourselves to real scalars. For \(\alpha \in \C\) we have \(d_{\alpha f} = \alpha d_f\), so if \(\alpha\) is not real then the sets \(\{A \in \C^{n \times n} : d_f(A) \in \R \} \) and \(\{A \in \C^{n \times n} : d_{\alpha f}(A) \in \R \} \) are disjoint.

Compare this to the set \(\{ x \in \C S_n \,:\, \exists y \in \C S_n ( x \leq_S y) \}\), which is all of \(\C S_n\).
\end{remark}

Fix a group \(G\). A \textbf{matrix representation} of \(G\) over \(\C\) (or \(\C\)-representation of \(G\)) is a homomorphism from \(G\) into GL\(_m(\C)\) for some positive integer \(m\). If \(\rho\) is a \(\C\)-representation of \(G\), \(tr\circ\rho\) is called a \(\C\)-\textbf{character} of \(G\). %
A \(\C\)-character of a group is necessarily constant on conjugacy classes, %
since if \(\sigma,\tau,\alpha \in G\) with \(\sigma = \alpha^{-1}\tau\alpha\) then for every \(\C\)-representation \(\rho\) of \(G\) we have \(\rho(\sigma) = \rho(\alpha)^{-1}\rho(\tau)\rho(\alpha)\) and similar matrices have the same trace. 

A \(\C\)-character that cannot be expressed as the sum of two other \(\C\)-characters is called \textbf{irreducible}. %
The set of irreducible \(\C\)-characters of \(G\) is abbreviated \labelx{notation:Irr}\(\Irr H\). %
If \(\chi\) is a \(\C\)-character of \(G\) then \(\chi(\e) \geq 0\) \cite[8.5]{rrhreps}, so \labelx{dcbar}\(\bar{d}_\chi\) is defined.

For a symmetric group \(S_n\) we can generate the set of irreducible characters using a construction on Young tablueaux. If \(\pi\) is a partition of \(n\), a \textbf{Young diagram} of shape \(\pi\) is a collection of boxes arranged in rows such that the number of boxes in row \(i\) is \(\pi_i\). Given a Young diagram with \(n\) boxes, assembling the integers \(\{1,\dots,n\}\) within them yields a \textbf{Young tableau}.\\
\begin{figure}[h]
\center{\yng(4,2,2,1)}
\caption{A Young diagram of shape \([4,2,2,1]\).}
\end{figure}
\begin{figure}[h]
\center{\young(3957,18,62,4)}
\caption{A Young tableau of shape \([4,2,2,1]\).}
\end{figure}

Fix an integer \(n\) and let \(\alpha\) be a Young tableau with size \(n\). Define \(R_\alpha = \{\sigma \in S_n : \forall i \in \{1,\dots,n\},\, \sigma(i)\text{ shares a row with \(i\) in }\alpha\}\) and \(C_\alpha = \{\sigma \in S_n : \forall i \in \{1,\dots,n\},\, \sigma(i)\) shares a column with \(i\) in \(\alpha\}\). The \textbf{Young symmetrizer} \(y_\alpha \in \C S_n\) is defined to be \[y_\alpha = \sum_{\substack{ \sigma \in R_\alpha \\ \tau \in C_\alpha }} \varepsilon(\tau) \sigma \tau ,\]
where \(\varepsilon\) is the sign function. The action of \(S_n\) on \(\C S_n y_\alpha\) forms a \(\C\)-representation of \(S_n\) and the corresponding character is irreducible. Furthermore, generating one such character for each partition of \(n\) yields the complete set of irreducible characters of \(S_n\) \cite[p.~52]{jamesandkerber}. We denote the irreducible character of \(S_n\) corresponding to the partition \(\alpha\) by \labelx{notation:oc}\oc{\alpha}. 

The character \oc{1,1,\dots,1} is the sign function \(\varepsilon\). The generalized matrix function \(d_\varepsilon\) is the determinant. The character \oc{n} is the constant function \(1 : S_n \rightarrow \C\) given by \((\sigma \mapsto 1)\). The corresponding function \(d_1\) is called the permanent. It is known that among the functions \(f \in \C S_n\) satisfying \(f \succeq 0\), \(\varepsilon\) is a minimum.
\begin{theorem}[Schur {\cite[thm 7.3, p.~214]{merrisbook}}]
\label{thm:schur}
If \(G \leq S_n\) and \(\chi \in \Irr(G)\) then \(\chi \geq \varepsilon\).
\end{theorem}
The minimality of \(\varepsilon\) is an immediate consequence of two other results regarding generalized matrix functions.
\begin{theorem}[{\cite[ex 7.13, p.~219]{merrisbook}}]
\label{thm:irrplus}
If \(G\leq S_n\) and \(\chi \in \Irr(G)\) then \(\chi \succeq 0\).
\end{theorem}
\begin{theorem}[Watkins  {\cite[thm 7.6, p.~215]{merrisbook}}]
\label{thm:watkins}
If \(f \in \C S_n\) and \(f \succeq 0 \) then \(f \geq \varepsilon\).
\end{theorem}
\begin{proof}[Proof of Theorem \ref{thm:schur}]
Let \(G\) be a subgroup of \(S_n\)  and let \(\chi \in \Irr(G) \subseteq \C S_n\). Then by Theorem \ref{thm:irrplus} we have \(\chi \succeq 0\) and therefore by Watkins' theorem we know \(\chi \geq \varepsilon\).%
\end{proof}


Since \(\varepsilon\), which corresponds to the ``smallest'' partition of \(n\), is also minimal in a different set, this suggests that the character corresponding to the ``largest'' partition of \(n\) may be maximal in another way.

\begin{conjecture}[Lieb's conjecture, "Permanental Dominance conjecture") {\cite[p.~224]{merrisbook}}]
\label{conj:pd}
If \(G \leq S_n\) and \(\chi \in \Irr(G)\), then \(\chi \leq 1\).\end{conjecture}
%
%
\begin{conjecture}[Soules' conjecture {\cite[p.~224]{merrisbook}}]
Let \(G\leq S_n\) and \(\chi \in \Irr(G)\). Let \(\rho\) be a \(\C\)-representation of \(G\) such that \(\rho(\sigma)\) is unitary for every \(\sigma \in G\) and \(\chi = tr \circ \rho\). Let \(\delta_i(\sigma)\) be the \(i\)th diagonal entry of \(\rho(\sigma)\). Then%
\[\delta_i \leq 1.\]
\end{conjecture}


\input{../top_postamble.tex}
