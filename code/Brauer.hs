module Brauer where
import Control.Concurrent.MVar (newEmptyMVar, putMVar, takeMVar)

import Types
import Partition
import Tables
import Theorems
import PrettyPrint (pretty, PrettyPrint)
import Wrappers --(cgalsolve, convert_lp_mlp, read_cgal_results, preplp)
import Numeric.Matrix (
    Matrix, fromList, toList, dimensions,
    mapWithIndex, transpose, MatrixElement,
    select, numCols
    )
import Data.List (sort)
import Control.Concurrent (forkIO)
import System.IO

pr :: (PrettyPrint a) => a -> IO ()
pr = putStrLn . pretty

constraintMatrix n p = do
    d <- decompMatrix' n p
    return $ transpose (w * d)
    where
    b1 = basis1 n p
    q = thms n p
    w :: Matrix ET
    w = fromList . map (encodePV b1) $ q

check n p k = do
	a <- constraintMatrix n p
	let n' = brauerdim n p
	check' n p k a n'

check' n p k a n' = do
	let b = e n' k
	let s = System a b
	let name = concat ["lp/", show n, "/", show p, "/",show k]
	let checkfile = name ++ ".summary"
	completionToken <- newEmptyMVar
	forkIO $ do 
		r <- solvelp name s
		let v = verify s r
		writeFile checkfile . pretty $ v
		putMVar completionToken (reachable v)
		return ()
	return completionToken

{-		-- LP : A x = B
data Result_Summary = Summary
    (Matrix ET) -- Thm matrix A
    (Matrix ET) -- Goal vector B
    Bool        -- Goal reachable?
    (Matrix ET) -- if reachable: solution
		-- if unreachable: certificate
    Bool        -- multiplication checks out?
    -}
verify :: LP_System -> LP_Result -> Result_Summary
verify (System a b) r = Summary a b reachable v multCheck
    where
    (reachable, v, multCheck) = case r of
	Result Feasible   x' -> (True, x, multCheck)
		where
	        x = transpose . fromList $ [x'] 
	        multCheck = (a * x) == b
	Result Infeasible y' -> (False, y, multCheck)
		where
		yT = fromList [y']
		y = transpose yT
	        multCheck = matrixAll (>=0) (yT * a)
			    &&
			    matrixAll (<0) (yT * b) 

matrixAll :: (MatrixElement e) => (e -> Bool) -> Matrix e -> Bool
matrixAll f m = (all . all $ f) . toList $ m

e :: Int -> Int -> Matrix ET
e n k = fromList . map (:[]) . take n $ map f [1..]
	where f x = if x == k then 1 else 0

dim :: Matrix ET -> (Int,Int)
dim = dimensions

remove_empty_cols :: (MatrixElement e) =>
		    Matrix e -> Matrix e
remove_empty_cols = transpose . fromList . 
		    filter (not . all (==0)) . 
		    toList . transpose

submatrix :: (MatrixElement e) => Matrix e 
				-> (Int -> Bool)
				-> (Int -> Bool)
				-> Matrix e
submatrix m r c = fromList . splits rowlength $ l
	where
	rowlength = length . filter c $ [1 .. numCols m]
	l = select (\(a,b) -> r a && c b) m	

decompBlock n p k m = submatrix m r c
    where
    b1 = basis1 n p
    b2 = basis2 n p
    rblocks = map (blockNumber n p) b1
    cblocks = map (blockNumber n p) b2
    r n = rblocks !! (n - 1) == k
    c n = cblocks !! (n - 1) == k
    
splits :: Int -> [a] -> [[a]]
splits _ [] = []
splits n xs = let (l, rest) = splitAt n xs 
	    in l : splits n rest

check_in_sequence n p k a n' = do
	token <- check' n p k a n'
	takeMVar token

run = do
    let primes = [13,11,7,5,3]
    let groups = [6..11]
    let pairs = [(n,p) | p <- primes, n <- groups, n >= p]
    sequence . map (forkIO . uncurry go') $ pairs

go n p = go'' n p stdout
go' n p = do
    let summaryFile = concat ["lp/", show n, "/",show p,"_summ.txt"]
    withFile summaryFile WriteMode (go'' n p)
go'' n p handle = do 
	let n' = brauerdim n p
	let b1 = basis1 n p
	let b2 = basis2 n p
	let d = brauerdim n p
	a <- constraintMatrix n p
	let t = snd . dimensions $ a
	hPutStrLn handle . concat $ [
	    "n=",show n,"\n",
	    "p=",show p,"\n",
	    "There are ", show .length $b1, " characters\n",
	    "and ", show .length $ b2, " brauer characters.\n",
	    "There are ",show t," theorems comprising the constraint matrix\n",
	    "The blocks have sizes...\n", show $ blockSizes n p
	    ]
	passed <- sequence $ map (\k -> check_in_sequence n p k a n') [1..d]
	hPutStrLn handle "..."
	hPutStrLn handle . concat $ [show . length . filter id $ passed,
			"/",show . length $ passed]
	let failures = map fst . filter (not . snd) . zip [1..] $ passed
	sequence_ . map (hPrint handle) $ failures
