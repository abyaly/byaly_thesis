{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverlappingInstances #-}
module PrettyPrint(pretty, 
		   PrettyPrint,
		   ) where
import Numeric.Matrix (Matrix, toList, MatrixElement, dimensions)
import Data.List (transpose)
import Text.PrettyPrint.Boxes (Box, render, hsep, vcat, 
    center1, right, left, text, (<+>))
import Data.Ratio (Ratio, numerator, denominator)
import qualified Data.Map as M
import Types

class PrettyPrint a where
  pretty :: a -> String
    
instance PrettyPrint (Ratio Integer) where
  pretty r = displayFraction r

instance PrettyPrint Int where
  pretty i = show i

instance (PrettyPrint a) => PrettyPrint ([[a]]) where
  pretty m = render box
    where
    columns = (map . map) (text . pretty) . transpose $ m
    box = hsep 1 center1 .
	  map (vcat right) $
	  columns

instance (PrettyPrint a) => PrettyPrint ([a]) where
  pretty l = render box
    where
    box = vcat left $ map (text . pretty) l

instance (PrettyPrint a, MatrixElement a) => PrettyPrint (Matrix a) where
  pretty = pretty . toList


displayFraction :: Rational -> String
displayFraction r = case denominator r of
		    1 ->
			show . numerator $ r
		    _ ->
			concat [
			show . numerator $ r,
			"/",
			show . denominator $ r]

displayVector :: (Show a) => [a] -> String
displayVector v =  unwords . map show $ v

instance PrettyPrint Result_Summary where
    pretty (Summary a b feasible cert multCheck) = mergeLF . unlines $
    	["Multiplication check: " ++
	    case multCheck of
		True  -> "PASSED."
		False -> "FAILED.",
	"The linear program Ax = B, x >= 0"]
	++ case feasible of
	    True  -> ["has solution x =",
		pretty cert]
	    False -> ["has NO SOLUTION.", 
		"There is an infeasibility certificate y such that ",
	    	"y^T A >= 0 and y^T B < 0.",
		"y =",
		pretty cert]
	++ ["With A = ",
	pretty a,
	"With B = ",
	pretty b]

mergeLF = unlines . filter (/= "") . lines

instance PrettyPrint String where
    pretty = id

instance PrettyPrint LP_System where
    pretty (System a b) = cplexlp a b
cplexlp :: Matrix ET -> Matrix ET -> String
cplexlp a b = "Minimize\n" ++ 
            "0 x1\n" ++ 
            "Subject To\n" ++
            body ++
            "End\n"
            where
	    n = fst . dimensions $ a
            sig x = if x >= 0 then "+" else "-"
            display x = pretty . abs $ x
            var n = "x" ++ show n
            sdv x n = [sig x, display x, var n]
            fillrow r = concat $ zipWith sdv r [1..]
	    equals = take n (repeat "=")
	    vector = map pretty . concat . toList $ b
	    columns = (transpose . map fillrow . toList $ a) ++ 
		      [equals] ++
		      [vector]
	    body = pretty . transpose $ columns

instance PrettyPrint Partition where
    pretty (Partition p) = show p

instance PrettyPrint PartitionVector where
    pretty pv = combine list2
	where
	list1 = M.toList . cleanPV $ pv
	list2 = map showComponent list1
	combine [] = ""
	combine (x:xs) = foldl f x xs
	    where f a b = a ++ " + " ++ b
	showComponent (p, r) = pretty r ++ pretty p
	cleanPV x = M.mapMaybe f x
	f 0 = Nothing
	f a = Just a
