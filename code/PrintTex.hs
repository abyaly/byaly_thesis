module PrintTex where

import Tables
import Data.List (foldl', zipWith, group, foldl)
import Numeric.Matrix ( numCols, toList, fromList)
import Types (Partition(..))
import PrettyPrint (pretty)
import Partition
import Theorems

latex_decomp_matrix :: Int -> Int -> IO String
latex_decomp_matrix n p = do
  d <- decompMatrix' n p
  rows <- decompMatrixRowLabels n p 
  let nc = numCols d
      cols = take nc rows
      preamble = concat
        [ "\\(\\begin{array}{r|"
        , take nc (repeat 'c')
        , "}\n"
        , mkRow corner (map phi cols)
        , "\\\\\n\\hline\n" ]
      rowtext = catRows 
        . zipWith mkRow (map chi rows) 
        . (fmap.fmap) pretty 
        $ toList d
      postamble = "\n\\end{array}\\)%"
  return $ preamble ++ rowtext ++ postamble
  where
    corner = concat ["S_", show n, ", p=", show p] 
    mkRow left entries = foldl' (\a b -> concat [a,"&",b]) left entries
    catRows (x:xs) = foldl' (\a b -> concat [a,"\\\\\n",b]) x xs

compress :: Partition -> String
compress (Partition l) = concatWith ',' . fmap (uncurry ex) . groupAndCount $ l
  where  
  groupAndCount xs = fmap (\l -> (head l, length l)) $ group xs
  ex i j = case j of 
    1 -> show i
    _ -> concat [ show i, "^{", show j, "}"]
  concatWith c [] = []
  concatWith c (x:xs) = foldl (\a b -> concat [a,[c],b]) x xs 


chi :: Partition -> String
chi p = concat ["\\oc{", compress p, "}"]
phi :: Partition -> String
phi p =  concat 
  [ "\\rot{\\bc{"
  , compress p 
  , "}}" ]

d_chi :: Partition -> String
d_chi p = concat ["\\frac{1}{",prT $ degree p,"}d_\\oc{", compress p, "}"]
d_bar_phi p = concat ["d_\\bc{", compress p, "}"] 

prT :: Rational -> String
prT x = if x == 1 then "" else 
        if denominator x == 1 then show (numerator x) 
        else concat ["\\frac{",show $numerator x,"}{",show $ denominator x,"}"]

{-
:m +Data.Ratio
prT :: Rational -> String
let prT x = if x == 1 then "" else concat ["\\frac{",show $numerator x,"}{",show $ denominator x,"}"]

:m +Data.Map
let s = mkPate (basis1 6 3)
let slist = fmap Data.Map.toList s
let geq a b = concat [a, " \\geq ", b ]
let g = fmap (\l -> (fst $ l!!1, fst $ l!!0)) slist
let s' = Numeric.Matrix.fromList $ fmap (encodePV (basis1 6 3)) s
d <- decompMatrix' 6 3

let col1 = fmap (\(a,b) -> geq (chi a) (chi b)) g
let col2 = fmap (\(a,b) -> geq (d_chi a) (d_chi b)) g

let colsep = fmap (const "&") g
let lastcol = fmap (const "\\") g
let newline = fmap (const "%\n") g

:m +Data.List
let pr = putStrLn . pretty :: [[String]] -> IO ()

bpv :: Partition -> [Partition]
let ind p = (+1) $ fromJust $ elemIndex p (basis1 6 3)
let bpv p = Data.List.filter ((/=0).fst) $ zipWith (\k n -> (k,basis2 6 3 !! n)) (row (ind p) d)  [0..]
let zzz1 p = fmap (\(n,x) -> concat [prT n, d_bar_phi x]) $ bpv p
:{
let zzz2 p = case zzz1 p of
              [] -> ""
              (x:xs) -> Data.List.foldl (\a b -> a ++ " + " ++ b) x xs
:}
let zzz p = concat $ [prT (1 % (degree p)) , "\\left[", zzz2 p, "\\right]"]

let col3 = fmap (\(a,b) -> geq (zzz a) (zzz b)) g



pr $ Data.List.transpose [col1, colsep, col2, colsep, newline, col3 ,lastcol]


-}
