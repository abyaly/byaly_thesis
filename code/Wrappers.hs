module Wrappers {-(cgalsolve, 
                 convert_lp_mlp,
                 read_cgal_results,
                 preplp,
                 run_gap
                 )-} where
import Types (LP_Result(Result), 
              LP_Status(Feasible,Infeasible),
              LP_System(System),
              ET,
              Partition(Partition)                
              )
-- This module exports functions that call external programs
-- and wait for them to terminate
import System.Process (runCommand, waitForProcess, readProcess)
import System.IO (withFile)
import Text.Read (readMaybe)
import Numeric.Matrix(toList, fromList, Matrix)
import PrettyPrint (pretty)

--describe the linear programming problem in CPLEX LP format
--preplp :: MatrixElement a => Matrix a -> [a] -> String
preplp a b = pretty $ System a b

newtype GapCmd = GapCmd String

run_gap :: GapCmd -> IO String
run_gap (GapCmd s) = readProcess "gap" ["-q"] (s++";\nquit;") 
                    >>= return . clean
    where
    clean (x:y:xs) = case x of
                        '\\' -> case y of
                            '\n' -> clean xs
                            _    -> x: clean (y:xs)
                        _    -> x: clean (y:xs)
    clean z = z
                
gap_decomposition_matrix :: Int -> Int-> IO [[Int]]
gap_decomposition_matrix n p = do
    gapout <- run_gap . display . decomp $ gapmodtbl n p
    let matrix = read $ gapout
    return matrix
    where
    display = gaplift "Display"
    decomp = gaplift "DecompositionMatrix"

gap_decomposition_row_labels :: Int -> Int -> IO [Partition]
gap_decomposition_row_labels n p = do
    gapout <- run_gap . display . flip list second . charparams . ordtbl 
                $ gapmodtbl n p
    let ps = map Partition . read . concat . lines $ gapout
    return ps
    where
    display = gaplift "Display"
    list = gaplift2 "List"
    second = GapCmd "x -> x[2]"
    charparams = gaplift "CharacterParameters"
    ordtbl = gaplift "OrdinaryCharacterTable"

gap_formatted_decompMatrix :: Int -> Int -> 
                        IO ([Partition], [Partition], Matrix ET)
gap_formatted_decompMatrix = do
                undefined


gapmodtbl :: Int -> Int -> GapCmd
gapmodtbl n p = GapCmd . concat $ ["CharacterTable( \"S", show n,
                                    "\" ) mod ", show p]
--Display( DecompositionMatrix( CharacterTable( \"S6\" ) mod 3 ) );
gaplift :: String -> GapCmd -> GapCmd
gaplift s (GapCmd x) = GapCmd $ concat [s,"( ",x," )"]

gaplift2 :: String -> GapCmd -> GapCmd -> GapCmd
gaplift2 s (GapCmd x) (GapCmd y) = GapCmd $
                concat [s,"( ",x,", ", y, " )"]

convert_lp_mlp :: FilePath -> FilePath -> IO ()
convert_lp_mlp infile outfile =
    runCommand str >>= waitForProcess >> return ()
	where
	str = unwords ["glpsol --check --lp",infile,"--wmps",outfile,"> /dev/null"]

solvelp :: FilePath -> LP_System -> IO LP_Result
solvelp name system = do
    let lpfile = name ++ ".lp"
    let mpsfile = name ++ ".mps"
    let outfile = name ++ ".out"
    writeFile lpfile (pretty system)
    convert_lp_mlp lpfile mpsfile
    cgalsolve mpsfile outfile
    read_cgal_results outfile

cgalsolve :: FilePath -> FilePath -> IO ()
cgalsolve infile outfile =
    runCommand str  >>= waitForProcess >> return ()
	where
	str = unwords["./lpsolver/modified_lp","<",infile,">",outfile]

read_cgal_results :: FilePath -> IO LP_Result
read_cgal_results file = do
    s <- readFile file
    let l = dropWhile (not . isPrefix "status:") . lines $ s
    let status = (!!1) . words . head $ l
    if status == "OPTIMAL"
    then return . Result Feasible . parse_vector .
         tail . dropWhile (not . (=="variable values:")) $ l
    else return . Result Infeasible . parse_vector .
         tail $ l
    where
    isPrefix :: String -> String -> Bool
    isPrefix a b = a == take (length a) b

parse_vector :: [String] -> [ET]
parse_vector l = map (readFraction . (!!1) . words) l

readFraction :: String -> Rational
readFraction = r2 . map (\x -> if x == '/' then '%' else x)
    where
    r1 = readMaybe :: String -> Maybe Integer
    r2 :: String -> Rational
    r2 s = case r1 s of
            Just x -> toRational x
            Nothing -> read s

export_matrix_gap :: Matrix ET -> GapCmd
export_matrix_gap m = GapCmd . filter (/='"') . show . 
    (map.map) pretty . toList $ m
    
gap_nullspace_matrix :: Matrix ET -> IO (Matrix ET)
gap_nullspace_matrix m = do
    let f = gaplift "NullspaceMat"
    let m' = export_matrix_gap m
    result <- run_gap $ f m'
    putStrLn result
    let n = case (readMaybe result :: Maybe [[Int]]) of
            Nothing -> []
            Just x  -> x
    return . fromList . (map.map) toRational $ n
