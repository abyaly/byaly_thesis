module Theorems
where
import Types
import Partition
import Data.List(nub)
import Data.Map(keys,fromList)
import Data.Tree(Tree(Node),unfoldTree)
import Data.Maybe(mapMaybe)

toss :: Partition -> [Partition]
toss (Partition xs) =  mapMaybe f . (zip [0..]) $ xs where
  l = length xs
  f :: (Int, Int) -> Maybe Partition
  f (n,x) = if (x<= 1) then Nothing
                    else if (n+1 == l) then Just . Partition $ take n xs ++ (x-1):[1]
                    else if (xs !! (n+1) < x) then Just . Partition $ take n xs ++ (x-1):(drop (n+1) xs) ++ [1]
                    else Nothing 

toss' :: Partition -> [Partition]
toss' x = concat . tail . takeWhile (not . null) . 
        iterate (\z -> nub . concatMap toss $ z) $ [x]

branches :: Tree a -> [[a]]
branches (Node label []) = [[label]]
branches (Node label subforest) = map (label:) . concatMap branches $ subforest

tossTree :: Partition -> Tree Partition
tossTree = unfoldTree (\p -> (p, toss p))

prune :: (a -> Bool) -> Tree a -> Tree a
prune f (Node x ts) = Node x (concatMap g ts)
            where
            g (Node y rest) = if f y then [Node y []]
                                     else concatMap g rest
                                    

--blockToss :: Int -> Partition -> [Partition]
blockToss p x = nub . top . prune inBlock . tossTree $ x
        where
        core = pcore p x
        inBlock z = pcore p z == core
        leaf (Node a _) = a
        top (Node x xs) = map leaf xs

block_mkPate :: Int -> [Partition] -> [PartitionVector]
block_mkPate p ps = map mkVec . (concatMap f) $  ps
  where
  f ::  Partition -> [(Partition,Partition)]
  f x = zip (repeat x) (blockToss p x)

mkPate :: [Partition] -> [PartitionVector]
mkPate ps = map mkVec . (concatMap f) $  ps
  where
  f ::  Partition -> [(Partition,Partition)]
  f  p = zip (repeat p) (toss p) 
mkVec :: (Partition, Partition) -> PartitionVector
mkVec (p,q) = fromList [(p,  fromIntegral b'),(q, fromIntegral . negate $ a')]
    where
    a = degree p
    b = degree q
    g = gcd a b
    a' = div a g
    b' = div b g
mkSchur :: [Partition] -> [PartitionVector]
mkSchur ps = map (\x -> fromList [(x,1.0)]) ps

crossBlock :: Int -> PartitionVector -> Bool
crossBlock p v = length blocks > 1
    where
    blocks = nub . map (pcore p) . keys . cleanPV $ v

thms :: Int -> Int -> [PartitionVector]
thms n p = mkSchur s ++ mkPate s
    where
    s = basis1 n p
