quickhull paper

http://www.cs.princeton.edu/~dpd/Papers/BarberDobkinHuhdanpaa.pdf

Barber, C.B., Dobkin, D.P., and Huhdanpaa, H.T., "The Quickhull 
  algorithm for convex hulls," ACM Trans. on Mathematical Software,
  22(4):469-483, Dec 1996, http://www.qhull.org.

qhull source
http://www.qhull.org/download/qhull-2012.1-src.tgz


The portion ofthe CGAL manual pertaining to the linear program solver is attributed to:

@incollection{cgal:fgsw-lqps-00a,
  author = {Kaspar Fischer and Bernd G{\"a}rtner and Sven Sch{\"o}nherr and Frans Wessendorp},
  title = {Linear and Quadratic Programming Solver},
  publisher = {{CGAL Editorial Board}},
  edition = {{4.4}},
  booktitle = {{CGAL} User and Reference Manual},
  url = {http://doc.cgal.org/4.4/Manual/packages.html#PkgQPSolverSummary},
  year = 2000
}


There is an error on row [6,2,2,2]
The sum of the row is short by 131
Numbers usable to make up the shortage:
1,10,54,131,45,120
The solutions without any coefficient exceeding 3 are...
131
1+10+120
1+1+1+10+10+54+54
1+1+10+10+10+45+54
1+10+10+10+10+45+45


StringOfPartition:= function( part )
    local pair;
 
    part:= List( Reversed( Collected( part ) ),
                 pair -> List( pair, String ) );
    for pair in part do
      if pair[2] = "1" then
        Unbind( pair[2] );
      else
        pair[2]:= Concatenation( "{", pair[2], "}" );
      fi;
    od;
    return JoinStringsWithSeparator( List( part,
               p -> JoinStringsWithSeparator( p, "^" ) ), " \\ " );
  end;;

MyLaTeXMatrix:= function( symt, blocknr )
    local alllabels, rowlabels;
 
    alllabels:= List( CharacterParameters( OrdinaryCharacterTable( symt ) ),
                      x -> StringOfPartition( x[2] ) );
    rowlabels:= alllabels{ BlocksInfo( symt )[ blocknr ].ordchars };
 
    return LaTeXStringDecompositionMatrix( symt, blocknr,
               rec(  phi:= "\\varphi", rowlabels:= rowlabels ) );
 end;;
t:= CharacterTable( "S12" ) mod 3;
b:= 1;
Print( MyLaTeXMatrix( t, b ) );

