module Partition{- (basis1,
                  basis2,
                  brauerdim,
                  encodePV,
                  decodePV,
                  thms,
                  partitionOrder,
                  )-} 
where
import Types(
    ET,
    Partition(Partition),
    PartitionVector,
    )
import Prelude hiding (lookup)
import PrettyPrint (pretty)
import Data.Ratio (Rational)
import Data.Map (Map, fromList, toList, unionWith, lookup, mapMaybe, keys)
import Data.List (sortBy, group, nub, sort, elemIndex)
import qualified Numeric.Matrix as Matrix
import qualified Data.Ord as O
import qualified Data.Maybe as MayB


blockNumber :: Int -> Int -> Partition -> Int
blockNumber n p t = unwrap $ elemIndex (pcore p t) cores
    where
    cores = nub . map (pcore p) $ basis1 n p
    unwrap x = maybe 0 (+1) x

-- ~ blockSizes :: Int -> Int -> [Int]
blockSizes n p = map (\x -> length . filter (f x) $ b2) [1..numblocks]
    where 
    b2 = basis2 n p
    numblocks = length . nub . map (pcore p) $ b2
    f k t = blockNumber n p t == k
        
sortp :: Int -> [Partition] -> [Partition]
sortp p = sortBy (partitionOrder p)

partitionOrder :: Int -> Partition -> Partition -> Ordering
partitionOrder p = O.comparing g
    where
--    core t = let (Partition xs) = pcore p t in xs
    g x@(Partition z) = (O.Down . isRegular p $ x, length z, O.Down x)

mkPartitions :: Int -> [Partition]
mkPartitions n = map (Partition . reverse) (mkPartitions' n)
mkPartitions' 1 = [[1]]
mkPartitions' n = concatMap f $ mkPartitions' (n-1)
    where 
    bump (x:xs) = (x+1):xs
    tag xs = 1:xs 
    f p = tag p:(g p)
    g p@(x:xs) = case xs of
      [] -> [bump p]
      (y:ys) -> if y > x then [bump p] else []

isRegular :: Int -> Partition -> Bool
isRegular p (Partition xs) = all (<p) . (map length) . group $ xs

(**) :: ET -> PartitionVector -> PartitionVector
(**) x pv = fmap (*x) pv
(#) :: PartitionVector -> PartitionVector -> PartitionVector
(#) = unionWith (+)
cleanPV :: PartitionVector -> PartitionVector
cleanPV x = mapMaybe f x
    where
    f 0 = Nothing
    f a = Just a
-- A vector indexed by partitions
zero :: PartitionVector
zero = fromList []

degree :: Partition -> Integer
degree p@(Partition xs) = fac (sum xs) `div` (product . map toInteger $ hooklens)
  -- compute the degree of a character corresponding to a partition.
  where
  hooklens = MayB.mapMaybe (hooklen p) coords
  coords = concat $ zipWith f [1..] xs
  f a b = zip (repeat a) [1..b]
  fac n = product . map toInteger $ [1..n]
  hooklen :: Partition -> (Int, Int) -> Maybe Int
  hooklen p@(Partition xs) (i,j) =  do
	  c1 <- l i j p
	  c2 <- l j i (conj p)
	  return (1 + c1 + c2)
	  where
	  l :: Int -> Int -> Partition -> Maybe Int
	  l x y (Partition zs) = if length zs < x then Nothing
						     else if zs !! (x-1) < y then Nothing
						     else Just ((zs !! (x-1)) - y)
  conj :: Partition -> Partition
  conj (Partition xs) = Partition . takeWhile (>0) . (map f) $ [1..]
	where f n = length . (filter (>=n)) $ xs

--a basis consisting of partitions
--these correspond to ordinary characters
basis1 n p = sortp p . mkPartitions $ n
--a basis consisting of regular partitions
--these correspond to brauer characters
basis2 n p = filter (isRegular p) $ basis1 n p
brauerdim n p = length $ basis2 n p

pcore :: Int -> Partition -> Partition
pcore p (Partition rs) = Partition core
    where 
	  toBeta xs = let m = length xs in 
			zipWith (+) xs [m-1, m-2 .. 0]
	  fromBeta xs = let m = length xs in
			filter (>0) $ zipWith (-) xs [m-1, m-2 .. 0]
	  bs = toBeta rs
	  numbeads k xs = length . filter (\x -> x `mod` p == k) $ xs
	  newbeads k xs = take (numbeads k xs) [k, k+p ..]
	  core' = reverse . sort . 
		    concatMap (\k -> newbeads k bs) $ 
			[0 .. (p-1)] 
	  core = fromBeta core'

encodePV :: [Partition] -> PartitionVector -> [ET]
encodePV ps pv = map f $ ps
  where
  f p = MayB.fromMaybe 0 . lookup p $ pv
decodePV :: [Partition] -> [ET] -> PartitionVector
decodePV ps s = cleanPV $ fromList (zip ps s)
  -- convert between partitition-indexed vector and position-indexed vector
  -- over the chosen basis
