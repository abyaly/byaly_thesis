-- ~ buildMatrix :: MatrixElement e => Int ->
				  -- ~ Int -> 
				  -- ~ ((Int, Int) -> e) ->
				  -- ~ Matrix e
-- ~ buildMatrix m n f = mapWithIndex (const . f) (zeroMatrix m n)
   -- ~ where
   -- ~ zeroMatrix :: Int -> Int -> Matrix Int
   -- ~ zeroMatrix x y = fromList . take x . repeat . take y . repeat $ 0
{-block :: (MatrixElement e) => [Int] -> Matrix e -> Matrix e
block b m = submatrix m r c
    where
    r x = x `elem` b
    c x = True-}


-- ~ mapd :: Int -> Int -> PartitionVector -> Matrix.Matrix ET -> PartitionVector
-- ~ mapd n p pv d = decodePV b2 pv'
        -- ~ where
        -- ~ v1 = Matrix.fromList [encodePV b1 pv]
        -- ~ v2 = v1 * d
        -- ~ pv' = head . Matrix.toList $ v2
        -- ~ b1 = basis1 n p
        -- ~ b2 = basis2 n p
-- ~ has pv p = case lookup p $ pv of
            -- ~ Nothing -> False
            -- ~ _ -> True


-- ~ 
-- ~ permutationMatrix n f = buildMatrix n n g
	-- ~ where
	-- ~ g (i',j') = let 
		    -- ~ (i,j) = (i'+1, j'+1)
		    -- ~ --i' and j' index starting at 0
		    -- ~ --i and j index starting at 1
		    -- ~ in
		    -- ~ if i == f j then 1 else 0
-- ~ 
-- ~ mkTransposition :: Int -> Int -> Int -> Int
-- ~ mkTransposition i j = \x ->  if x == i then j
			-- ~ else if x == j then i
			-- ~ else x
-- ~ 
-- ~ decompMatrix :: Int -> Int -> Matrix ET
-- ~ decompMatrix 6 3 = fromList [
                       -- ~ [1,0,0,0,0,0,0], -- 6
		       -- ~ [1,1,0,0,0,0,0], -- 5 1
		       -- ~ [0,0,1,0,0,0,0], -- 4 2
		       -- ~ [0,1,0,1,0,0,0], -- 3 3
		       -- ~ [0,1,0,0,1,0,0], -- 4 1 1
		       -- ~ [1,1,0,1,1,1,0], -- 3 2 1
		       -- ~ [0,0,0,0,0,0,1], -- 2 2 1 1
		       -- ~ [1,0,0,0,0,1,0], -- 2 2 2
		       -- ~ [0,0,0,0,1,1,0], -- 3 1 1 1
		       -- ~ [0,0,0,1,0,1,0], -- 2 1 1 1 1
		       -- ~ [0,0,0,1,0,0,0]] -- 1 1 1 1 1 1
-- ~ decompMatrix 9 3 = l * m * r
	-- ~ where
	-- ~ t = mkTransposition 9 10
	-- ~ l = permutationMatrix 30 t
	-- ~ r = permutationMatrix 16 t
	-- ~ m = fromList [
		-- ~ [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], -- 9
		-- ~ [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0], -- 81
		-- ~ [0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0], -- 72
		-- ~ [0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0], -- 63
		-- ~ [0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0], -- 54
		-- ~ [0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0], -- 711
		-- ~ [1,1,0,1,0,1,1,0,0,0,0,0,0,0,0,0], -- 621
		-- ~ [0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0], -- 531
		-- ~ [0,0,0,1,1,0,1,0,1,0,0,0,0,0,0,0], -- 441**** row 9
		-- ~ [1,1,0,1,1,0,1,0,0,1,0,0,0,0,0,0], -- 522**** row 10
		-- ~ [0,1,0,1,1,1,1,0,1,1,1,0,0,0,0,0], -- 432
		-- ~ [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0], -- 5211
		-- ~ [0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0], -- 4311
		-- ~ [0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0], -- 4221
		-- ~ [1,1,0,0,0,1,1,0,1,1,1,0,0,0,1,0], -- 3321
		-- ~ [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1], -- 32211
		-- ~ [0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0], -- 333
		-- ~ [0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0], -- 6111
		-- ~ [1,1,0,0,0,0,0,0,0,1,0,0,0,0,1,0], -- 3222
		-- ~ [0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0], -- 51111
		-- ~ [0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0], -- 42111
		-- ~ [1,0,0,0,1,0,1,0,1,1,0,0,0,0,1,0], -- 33111
		-- ~ [1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0], -- 22221
		-- ~ [0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0], -- 411111
		-- ~ [0,0,0,0,1,0,0,0,1,1,1,0,0,0,1,0], -- 321111
		-- ~ [0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0], -- 222111
		-- ~ [0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0], -- 3111111
		-- ~ [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0], -- 2211111
		-- ~ [0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0], -- 21111111
		-- ~ [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]] -- 111111111
	       -- ~ --1 1 2 1 1 1 1 3 1 1 1 4 4 2 1 5
-- ~ decompMatrix n p = undefined
    -- ~ 
-- ~ verify n p k = do
    -- ~ let file = concat [
		-- ~ "lp/",
		-- ~ show n,
		-- ~ "/",
		-- ~ show p,
		-- ~ "/",
		-- ~ show k,
		-- ~ ".out"]
    -- ~ s <- readFile file
    -- ~ let l = drop 1 . lines $ s
    -- ~ let status = (!!1) . words . (!!0) $ l
    -- ~ if status == "OPTIMAL"
	-- ~ then cleanSolution (drop 3 l) n p k
	-- ~ else cleanNonSolution s n p k

-- ~ cleanSolution :: [String] -> Int -> Int -> Int -> IO [ET]
-- ~ cleanSolution cs n p k = do
    -- ~ putStrLn "We have a nonnegative solution the linear system m * x = v"
    -- ~ putStrLn $ "with b = " ++ (show v) ++ "^T,"
    -- ~ putStrLn $ "and with m = "
    -- ~ putStrLn . pretty . toList $ m
    -- ~ return coefficients
    -- ~ where
    -- ~ coefficients = map readFraction . concatMap (drop 1 . words) $ cs
    -- ~ m = constraintMatrix n p
    -- ~ v = e dim k
-- ~ 
-- ~ convert_b1_b2 :: Int -> Int -> PartitionVector -> PartitionVector
-- ~ convert_b1_b2 n p v = v'
    -- ~ where
    -- ~ b1 = basis1 n p
    -- ~ b2 = basis2 n p
    -- ~ v1 = fromList [encodePV b1 v]
    -- ~ d = decompMatrix n p
    -- ~ v2 = v1 * d
    -- ~ v' = decodePV b2 . concat . toList $ v2
-- ~ 
-- ~ cleanNonSolution _ _ _ _ = return []


{- use this to pipe stuff to quickhull and read the output. In System.Process
readProcess::
 FilePath	command to run
-> [String]	any arguments
-> String	standard input
-> IO String	stdout + stderr 
-}

{-
blocks :: Int -> Int -> [Int]
blocks 9 3 = [1,1,2,1,1,1,1,3,1,1,1,4,4,2,1,5]
blocks _ _ = undefined

block :: Int -> Int -> Int -> Matrix ET -> Matrix ET
block i n p m = fromLists . map (map snd) . pruneCols . pruneRows . map (zip b) . toLists $ m 
	where
	b = blocks n p
	pruneRows = filter (all (\(x,y) -> x == i || y == 0))
	pruneCols = map (filter (\(x,y) -> x == i))


qhullf :: Matrix ET -> IO ()
qhullf m = qhull m >>= writeFile "out"
	
getFacets :: String -> [[ET]]
getFacets s = mapMaybe f (parseFacets rest)
	where
	(dim':num':rest) = lines s
	dim :: Int
	dim = read dim'
	f (xs,z) = case z of
		[] -> Nothing
		y:_ -> if abs y > 1e-14 
			then Nothing
			else Just xs
	parseFacets :: [String] -> [([ET],[ET])]
	parseFacets s = map (splitAt (dim - 1) . (map read) . words) $ s 
getFacetsv :: String -> [[Integer]]
getFacetsv s = filter (has0) . map (map read) . map words $ rest
	where
	(n:rest) = lines s
	has0 xs = 0 `elem` xs

prepQhull :: Matrix ET -> String
prepQhull m = dim ++ "\n" ++ numpts ++ "\n" ++ pts
  where
    xs = toLists m
    dim = show . length . head $ xs
    numpts = show . (+1) .length $ xs
    pts = unlines . (map displayVector) $ (zero:xs)
    zero = map (const 0) . head $ xs


qhull :: Matrix ET -> IO String
qhull m = do
  let command = "qconvex"
  let input = prepQhull m
  let args = ["n"]
  output <- readProcess command args input
  return output

qhullv :: Matrix ET -> IO String
qhullv m = do
  let command = "qconvex"
  let input = prepQhull m
  let args = ["i"]
  output <- readProcess command args input
  return output

glpsolve infile outfile = runCommand str -- >> return ()
	where
	str = unwords ["glpsol --lp",infile,"--exact -o",outfile]
-}
