module Tables --(decompMatrix, checkMatrix)
where
import System.IO
import Types (Partition, ET)
import Numeric.Matrix (Matrix, fromList, det, transpose)
import qualified Data.List as L
import Data.Function (on)
import Partition(partitionOrder)
import Wrappers

{-decompMatrix :: Int -> Int -> IO (Matrix ET)
decompMatrix n p = do
    let filepath = concat ["tables/", show n, "_", show p, ".txt"]
    (r,c,m) <- loadTable filepath
    let ordering = partitionOrder p
    return . assembleMatrix . sortMatrix ordering r c $ m-}

decompMatrix' :: Int -> Int -> IO (Matrix ET)
decompMatrix' n p = do
    m <- gap_decomposition_matrix n p
    r <- gap_decomposition_row_labels n p
    let ordering = partitionOrder p
    return . assembleMatrix . sortMatrix' ordering r $ m

decompMatrixRowLabels :: Int -> Int -> IO [Partition]
decompMatrixRowLabels n p = 
    fmap (L.sortBy $ partitionOrder p)
    $ gap_decomposition_row_labels n p

parsePartition :: String -> Partition
parsePartition s = read $ "Partition [" ++ s' ++ "]"
    where
    s' = map spacetocomma . unwords . map g . words $ s
    spacetocomma x = if x == ' ' then ',' else x
    g :: String -> String
    g x = unwords . take m . repeat $ x'
        where
        z = splitList '.' x
        x' = head z
        m = case length z of
                1 -> 1
                2 -> read . (!!1) $ z
    
splitList :: (Eq a) => a -> [a] -> [[a]]
splitList _ [] = []
splitList y xs = run : splitList y rest'
        where
        (run, rest) = span (/=y) xs
        rest' = case rest of
                    [] -> []
                    _  -> tail rest

loadTable :: FilePath -> IO ([Partition],[Partition],[[Int]])
loadTable file = readFile file >>= return . parseTable

assembleMatrix :: [[Int]] -> Matrix ET
assembleMatrix = fromList . (map . map) toRational
sortMatrix :: (Partition -> Partition -> Ordering) ->
              [Partition] -> -- rows
              [Partition] -> -- columns
              [[Int]] ->
              [[Int]]
sortMatrix order r c m = sortRows .  sortCols $ m
    where
    sortRows = sortWith r
    sortCols = map (sortWith c)
    sortWith partitions list = 
        map snd . L.sortBy (order `on` fst) . zip partitions $ list


sortMatrix' order rowLabels m = sortCols . sortRows $ m
    where
    sortRows = sortWith rowLabels
    sortWith partitions list = 
        map snd . L.sortBy (order `on` fst) . zip partitions $ list
    sortCols list = L.transpose . 
                    L.sortBy (compare `on` L.elemIndex 1) .
                    L.transpose $ list

parseTable :: String -> ([Partition],[Partition],[[Int]])
parseTable s = (rows,columns,entries)
    where
    (rowsection, rest) = span (/= "Columns:") . tail . lines $ s
    (columnsection, rest') = span (/= "Entries:") . tail $ rest
    entrysection = tail rest'
    rows = map parsePartition rowsection
    columns = map parsePartition columnsection
    entries = parseMatrix (length rows) (length columns) entrysection
    
parseMatrix :: Int -> Int -> [String] -> [[Int]]
parseMatrix r c s = take r . map (readrow c . extend) $ s
    where
    extend s = s ++ repeat ' '
    readrow :: Int -> String -> [Int]
    readrow 0 _ = []
    readrow m (x:_:xs) = parse x : readrow (m-1) xs
    parse :: Char -> Int
    parse x = case x of
                ' ' -> 0
                _ -> read [x]

--The determinant of the cartan matrix should be a power of p
sanityCheck1 :: Matrix ET -> Integer -> Bool
sanityCheck1 d p = p' ^ exponent == n
    where
    p' :: ET
    p' = (fromIntegral p)
    c = transpose d * d
    n = det c :: ET
    exponent :: Integer
    exponent = floor $ logBase (fromRational p') (fromRational n)
--d * y = x
--where 
--d is the decomposition matrix
--y is a vector consisting of the degrees of the brauer characters
--x is a vector consisting of the degrees of the ordinary characters
sanityCheck2  = undefined
