{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Types where
import Numeric.Matrix (Matrix)
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Data.List (elemIndex)

type ET = Rational

newtype Partition =  Partition [Int] deriving (Eq, Show,Ord,Read)
-- a partition of n is a descending sequence 
-- of integers with sum n.

type PartitionVector = Map Partition ET
-- a partition vector is a map
-- to a c
		-- LP : A x = B
data Result_Summary = Summary {
    a :: (Matrix ET), -- Thm matrix A
    b :: (Matrix ET), -- Goal vector B
    reachable :: Bool,        -- Goal reachable?
    v :: (Matrix ET), -- if reachable: solution
		-- if unreachable: certificate
    multcheck :: Bool        -- multiplication checks out?
    }
data LP_Status = Feasible | Infeasible deriving (Show)
data LP_Result = Result {
    resultStatus :: LP_Status,
    resultVector :: [ET]
    } deriving (Show)

--Describes a linear system Ax=B
data LP_System = System (Matrix ET) (Matrix ET)
--			A	     B
