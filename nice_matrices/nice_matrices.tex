\input{../top_preamble}
Since Conjecture \ref{conj:naiveschur} and Conjecture \ref{conj:naivepd} are false, it looks like a dominance relation on the entire set of positive semidefinite matrices is too much to ask for. However some of these inequalities may reappear if we restrict our attention to a subset.

\rrh{Let} \(p\) \rrh{be} a prime number. We define a positive semidefinite matrix \(A \in \C^{n\times n}\) to be \textbf{\nice{p}} if it satisfies
\[\prod_{i =1}^na_{i, \sigma(i)} = 0\]
for every \(p\)-singular \(\sigma \in S_n\). We denote the set of \nice{p} matrices in \(\C ^ {n \times n}\) by \labelx{notation:pmat}\(\pmat_n(p)\). When \(n\) and \(p\) are clear from context, we will elide them and write \(\pmat\) instead. We find this set of interest because if \(A\) is \nice{p}, then
\[d_{\chi}(\text{A}) = d_{\res{\chi}}(\text{A}),\]
where \labelx{notation:chi'}\(\res{\chi}\) is the restriction of \(\chi\) to \(p\)-regular group elements. As a consequence of this we have the following.
\begin{theorem}
Let \(p\) be a prime and let \(f,g \in \C S_n\) with \(f \succeq g\). Then %
\[\res{f} \succeq_{\pmat_n(p)} \res{g}.\]
\end{theorem}
\begin{proof}On the set \(\pmat_n(p)\), the functions \(d_f\) and \(d_{f'}\) are equal.\end{proof}
The the statement that a matrix \(A\) is \nice{p} is a statement that ``enough'' of the off-diagonal entries of \(A\) are zero. If they are \textit{all} zero or if there is a zero row or column the property is trivially satisfied. We observe that matrices of this nature are in fact \nice{p} for every prime \(p\).
\begin{theorem}
If \(p\) is a prime and \(A \in \C ^{n \times n}\) is a diagonal matrix with nonnegative entries then \(A \in \pmat_n(p)\).
\end{theorem}\begin{proof}For any \(p\)-singular \(\sigma \in S_n\), we have \(\sigma \neq \e\), so the product \(\pi_{i=1}^na_{i\sigma(i)}\) contains an off-diagonal (zero) entry of \(A\).\end{proof}
\begin{theorem}
If \(p\) is a prime, \(B \in \C ^{(n-1) \times (n-1)}\) is positive semidefinite, and \(P \in \C ^{n \times n}\) is a permutation matrix then \(A = P^{-1}(B \oplus 0) P \in \pmat_n(p)\). 
\end{theorem}\begin{proof}For \(\sigma \in S_n\) the product \(\pi_{i=1}^na_{i\sigma(i)}\) has an entry from every row of \(A\), one of which is a zero row.\end{proof}
Addtionally, the set \(\pmat_n(2)\) is somewhat degenerate. All \nice{2} matrices are of one of these two types.
\begin{theorem}
\label{thm:p2}
If \(A \in \pmat_n(2)\) then either \(A\) is a diagonal matrix or \(A = P^{-1} (B \oplus 0) P\), where \(B\) is positive semidefinite and \(P\) is a permutation matrix.
\end{theorem}
\begin{proof}
Suppose \(A \in \pmat_n(2)\) is not a diagonal matrix. Then for some \(i \neq j\) there is an entry \(a_{ij}\) of \(A\) that is nonzero, and since \(A\) is Hermetian\rrh{,} \(a_{ji}\neq 0\) as well. %
Put \(\sigma = (ij) \in S_n\). Since \(\sigma\) is 2-singular, we have \[\prod_{i =1}^na_{i, \sigma(i)} = 0.\] But this is a product of \(a_{ij}\) and \(a_{ji}\) and diagonal entries of \(A\). Since we know \(a_{ij}\) and \(a_{ji}\) are nonzero, some diagonal entry \(a_{kk}\) must be zero.

We claim that every entry on row \(k\) or column \(k\) is zero as well. Since \(A \geq 0\), for any integer \(l \in 1,\dots,n\) we have that the principal minor \(a_{ll}a_{kk} - a_{kl}a_{lk} \geq 0\), so%
\[ - |a_{kl}|^2 = - a_{kl}a_{lk} = a_{ll}a_{kk} - a_{kl}a_{lk} \geq 0. \]
Thus there is a permutation matrix \(P\) such that \(A = P^{-1}CP\), where the last column and last row of \(C\) are zero. We have \(C = C(n|n) \oplus 0\), and \(C(n|n)\) is positive semidefinite because it is a principal submatrix of \(C\). \end{proof}



We will now turn our attention to an analogue of Schur's conjecture involving \(\leq_\pmat\). 

\begin{conjecture}
\label{conj:schurforibr}
Let \(n\) be a positive integer and let \(p\) be a prime. If \(G \leq S_n\) and \(\phi \in \IBr(G)\), then \(\phi \geq_\pmat \varepsilon\).
\end{conjecture}

We present some cases in which Conjecture \ref{conj:schurforibr} holds. In particular, we will see it holds if \(p = 2\) or if the group \(G\) is \(p\)-solvable. (The group \(G\) is called \(p\)-\textbf{solvable} if the nonabelian composition factors of \(G\) have order relatively prime to \(p\). In particular, this is true if the order of \(G\) is relatively prime to \(p\).) We will eventually show that it also holds for \(G=S_n\) with \(n \leq 6\) and for several of the irreducible Brauer characters of larger symmetric groups.

\begin{theorem}[Fong -- Swan {\cite[thm 72.1]{dornhoffb}}]
\label{thm:fongswan}
Let \(G\) be a \(p\)-solvable group. Let \(\phi \in \IBr(G)\). There exists an irreducible character \(\chi\) of \(G\) such that \(\res{\chi}=\phi\).
\end{theorem}
\begin{theorem}
Let \(G\) be a \(p\)-solvable group, and let \(\phi \in \IBr(G)\). Then there is a \(\chi \in Irr(G)\) such that \(d_\chi(A)=d_\phi(A)\) for all \nice{p} matrices \(A\).
\end{theorem}
\begin{proof}
By the Fong -- Swan theorem there exists a \(\chi \in \Irr(G)\) with \(\res{\chi} = \phi\). If \(A\) is a \nice{p} matrix, \(d_\chi(A) = d_{\res{\chi}}(A) = d_\phi(A)\).
\end{proof}
\begin{corollary}
If \(G\) is a \(p\)-solvable group and \(\phi \in \IBr(G)\) then \(\phi \geq_\pmat \varepsilon\).
\end{corollary}
\begin{proof}
\rrh{Let \(G\) be a \(p\)-solvable group and let \(\phi\in\IBr(G)\).  By Theorem \ref{thm:fongswan}, \(\phi=\chi'\) for some \(\chi\in\Irr(G)\).}
 Then for \(A\in \pmat\) we have \(\bar{d}_\phi(A) = \bar{d}_{\res{\chi}}(A) = \bar{d}_\chi(A) \geq \det(A)\)\rrh{, where the last step is from Theorem \ref{thm:schur}}.
\end{proof}
So we see that for a \(p\)-solvable group \(G\), the partial order \((\IBr(G),\leq_\pmat)\) is order isomorphic to a subset of \((\Irr(G),\leq)\).

Given a Hermetian \(A \in \C^{n\times n}\), define \(G(A)\) to be the simple graph with vertex set \(\{v_1, v_2, \dots , v_n\}\) and edge set \(\{\{v_i,v_j\}: a_{ij} \neq 0\}\).

\begin{theorem}
\label{thm:nicematrixcharacterization}
Suppose \(A \in \C^{n\times n}\) and \(p \geq 3\) is a prime. The following are equivalent:
\begin{enumerate}
\item \(A\) is \nice{p}.
\item \(G(A)\) has no cycles of length divisible by \(p\) and \(A \geq 0\).
\item There is a simple graph \(G\) with vertex set \(V(G) = \{v_1, \dots , v_n\}\) that has no cycles of length divisible by \(p\) and \(A = W^* W\) for some (possibly non-square) complex matrix \(W=[w_1|w_2|\dots|w_n]\) with the vectors \(w_i\) and \(w_j\) orthogonal exactly when \(v_i\) and \(v_j\) are not adjacent in G.
\end{enumerate}
\end{theorem}
\begin{proof}

(\(1 \Rightarrow 2\)): (contrapositive) Suppose \(A\) is positive semidefinite and suppose \(G(A)\) has a cycle \(c\) with length \(k\) divisible by \(p\). Then \(c = (v_{f(1)}, v_{f(2)}, \dots , v_{f(k)})\) for some (injective) function \(f:\{1,\dots,k\}\to\{1,\dots,n\}\). Let \(\sigma\) be the cycle \((f(1),f(2),\dots,f(k))\) in \(S_n\). This is a \(p\)-singular permutation, but the \rrh{factors in the} product
\[\prod_{i =1}^na_{i, \sigma(i)} = \left[\prod_{i =1}^ka_{f(i), f(i+1 \operatorname{mod} k)}\right]\prod_{i \not\in \im(f)}a_{ii}\]
\rrh{are either} diagonal entries of \(A\) \rrh{or} entries of \(A\) corresponding to edges in \(G(A)\). All of these are nonzero, so the product is nonzero and \(A\) is not \nice{p}.\\
(\(2 \Rightarrow 3\)): Assume (2). Since \(A \geq 0\), there is a matrix \(W \in C^{n\times n}\) such that \(A = W ^* W\). Let \(\{w_1, \dots , w_n\}\) be the columns of \(W\), and let \(G = G(A)\).\\
(\(3 \Rightarrow 1\)): Assume (3). Let \(\sigma \in S_n\) be \(p\)-singular, and decompose \(\sigma\) into disjoint cycles. Since the degree of \(\sigma\) is the least common multiple of the cycle lengths, and since \(p\) divides the degree of \(\sigma\), \(p\) must divide one of the cycle lengths. Let \(d\) be a component cycle with length divisible by \(p\). Now, the vertices in \(G\) indexed by the integers of \(d\) do not form a cycle. Therefore there is an integer \(k\) such that \(v_k\) is not adjacent to \(v_{d(k)} = v_{\sigma(k)}\). So \(w_k \perp w_{\sigma(k)}\). This means that
\[a_{k, \sigma(k)} = \sum_{i=1}^m[W^*]_{ki}W_{i\sigma(k)}%
= \sum_{i=1}^m \overline{W_{ik}} W_{i\sigma(k)} = \langle w_{\sigma(k)}, w_k\rangle =0,\]
so
\[\prod_{i =1}^na_{i, \sigma(i)} = 0,\]
and \(A\) is a \nice{p} matrix.
\end{proof}
\begin{theorem}\label{thm:vectorgraph}
Let \(G\) be a simple graph with vertices \(v_1,\dots,v_n\). Then there exists a basis \(x_1,\dots,x_n\) of \(\C^n\) such that \(E(G)=\{\{v_i,v_j\} : x_i \not\perp x_j, i \neq j\}\).
\end{theorem}
\begin{proof}
We prove this by well ordering. Suppose there exists a graph for which this theorem is false, and the graph \(G\) is a counterexample with the number of vertices \(n\) minimal. Then \(G\) cannot be the empty graph. Let \(H\) be the graph induced by removing the vertex \(v_n\) from \(G\). There exists a basis \(X=\{x_1,\dots,x_{n-1}\}\) of \(\C^{n-1}\) such that  \(E(H)=\{\{v_i,v_j\} : x_i \not\perp x_j, i \neq j\}\).
Put \(B = \{ v : \{v,v_n\} \in E(G) \} \) and \(A = X \backslash B \). We will produce a vector \(x \in \C^n\) such that \(X \cup \{x\}\) is a basis for \(\C^n\), \(x \perp v\) for all \(v \in A\), and \(x \not\perp v\) for all \(v \in B\).

Let \(m\) be the Lebesgue measure on \(A^\perp\). We know that \(X^\perp = (A \cup B)^\perp\) is a one dimensional subspace of \(A^\perp\), so \(\langle A \cup B\rangle \cap A^\perp\) has codimension one as a subspace of \(A^\perp\). Thus \(m(\langle X \rangle \cap A^\perp)=0.\)

Let \(b \in B\). Consider the dimension of \(b^\perp \cap A^\perp\). Clearly it is no greater than \(\dim A^\perp\), and if they were equal we would have%
\begin{align*}
b^\perp \cap A^\perp &= A^\perp, \\
b^\perp &\supseteq A^\perp,\\
(b^\perp)^\perp &\subseteq (A^\perp)^\perp, \\
\langle b \rangle &\subseteq \langle A \rangle.%
\end{align*}%
However, \(\langle B \rangle \cap \langle A \rangle = \emptyset\), so this is not the case. Therefore \(\dim(b^\perp \cap A^\perp) < \dim(A^\perp) \) and \(m(b^\perp \cap A^\perp)=0\). %
This gives us that \(S=\left(\langle X \rangle \cup \bigcup_{b\in B} b^\perp \right) \cap A^\perp\) is a finite union of measure zero sets. It cannot be all of \(A^\perp\), so there is a vector \(x\in A^\perp \backslash S\).

Since \(x \in A^\perp\) we know that \(x \perp v\) for every \(v \in A\). Also \(x \not\in S\), which contains every vector of \(A^\perp\) that is orthogonal to an element of \(B\) or is in the span of \(X\). We have that \(X \cup \{x\}\) is a basis of \(\C^n\) with the desired property.\end{proof}

Using Theorem \ref{thm:nicematrixcharacterization} and Theorem \ref{thm:vectorgraph}, we may now describe a procedure for creating \nice{p} matrices.
\begin{itemize}
\item Pick a graph \(G\) with \(n\) vertices and no cycles of length divisible by \(p\).
\item Pick vectors \(v_1,\dots,v_n\in\C^n\) such that \(v_i\) and \(v_j\) are orthogonal exactly when \(i\) and \(j\) are not adjacent in \(G\). 
\item Set \(V=[v_1|v_2|\dots|v_n]\) and \(A=V^*V\).
\end{itemize}
\rrh{The matrix \(A\) is \(p\)-regular by Theorem \ref{thm:nicematrixcharacterization}.}

Example for \(p=3\) and \(n=4\):
\[\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={circle,fill=red!15}]
  \node (n1) at (1,1) {1};
  \node (n2) at (1,3)  {2};
  \node (n3) at (3,1)  {3};
  \node (n4) at (3,3) {4};
  \foreach \from/\to in {n1/n2,n1/n3,n2/n4,n3/n4}
    \draw (\from) -- (\to);
\end{tikzpicture}\]
\[%
v_1 =%
\left(\begin{array}{c}
i\\
2\\
0\\
0
\end{array}\right)%
,v_2 =%
\left(\begin{array}{c}
3\\
0\\
2\\
0
\end{array}\right)%
,v_3=%
\left(\begin{array}{c}
0\\
1\\
0\\
1
\end{array}\right)%
,v_4=%
\left(\begin{array}{c}
0\\
0\\
2\\
3
\end{array}\right)%
\]
\[A =%
\left(\begin{array}{cccc}%
5 & -3i & 2 & 0\\
3i & 13 & 0 & 4\\
2 & 0 & 2 & 3\\
0 & 4 & 3 & 13
\end{array}\right)\]

\input{../top_postamble}
