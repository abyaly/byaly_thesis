\input{../top_preamble}
\chapter{Further Work}
\label{chptr:FurtherWork}
We have used Pate's theorem (\ref{thm:pate}) to generate most of the dominance inequalities we used on \(\C S_n\). However, the continued work of T.~H.~Pate includes many more results of this nature, a few of which we will mention here. Pushing these through the decomposition map would surely yield more relations between Brauer characters.

\begin{theorem}[Pate \cite{pate1999}]
\label{thm:patepd}
If \(\alpha\) is a partition of \(n\) of the form \((p,q^w,2^s,1^t)\) where \(p , q , s , t, \)
and \(w\) are non-negative integers and \(0 \leq w \leq 2\), then \(\oc{\alpha} \leq \oc{n}\).
\end{theorem}

\newcommand{\lleq}{<\!\!\!<}
These next results are about the ordering \(\lleq\) on partitions of \(n\). Given partitions  \(\alpha, \beta\) of \(n\) we say \(\alpha \lleq \beta\) if for every partition \(\omega\) of \(m\) such that the sequence concatenations \((\omega,\alpha)\) and \((\omega,\beta)\) are partitions of \(m + n\) we have \(\oc{(\omega,\alpha)} \leq \oc{(\omega,\beta)}\). We extend the definition to include the case where \(\omega\) is the empty sequence, so \(\alpha \lleq \beta\) implies \(\oc{\alpha} \leq \oc{\beta}\).

\begin{theorem}[Pate {\cite[thm.~14]{pate1998}}]
\label{thm:patec1}
Suppose \(\alpha = (\alpha_1,\dots,\alpha_s)\) is a partition of \(n\). For \(0 \leq i < s\), \rrh{d}efine \(\beta_i = \left(\alpha_1, \alpha_2, \dots , \alpha_i, \sum_{j=i+1}^s\alpha_j\right) \). If \(\beta_i\) is a partition of \(n\) for every such \(i\), we have
\[ \alpha = \beta_{s-1} \lleq\rrh{\cdots}\lleq \beta_1 \lleq \beta_0 = (n).\]
\end{theorem}

\begin{theorem}[Pate {\cite[thm.~7]{pate1999}}]
\label{thm:patec2}
If \(n, p\) and \(k\) are positive integers such that \(p \geq n + k\), then \((p, n^{k+1}) \lleq (n + p, n^k)\).
\end{theorem}

Consider the partition \(\alpha = (4,2^2)\). By Theorem \ref{thm:patec1} we have \((4,2^2) \lleq (4^2) \lleq (8)\), thus \(\oc{4,2^2} \leq \oc{4^2} \leq \oc{8}\). These inequalities were not included as basis vectors for the convex cone we generated, so including them may pick up vectors we previously missed.

The \rrh{approach} used in this dissertation is unlikely to scale well to larger groups. Unfortunately the worst case time complexity of the simplex method, typically used to find a solution to a linear program, is not very good. There exist examples (\cite{murty1983linear}) of linear programs where the time complexity is exponential in the size of the problem. While the linear programs we constructed do not necessarily achieve the worst case performance -- in fact it is clear that in many cases they did not -- no care was taken to prevent this from happening. We describe a technique by which we may generate convex cones such that testing vectors for membership has better worst case performance. However the trade off is that since these are subsets of the convex cones we previously considered we may miss vectors that we previously might have picked up.

\rrh{Let \(V_1\) and \(V_2\) be vector spaces and put} \(V = V_1 \oplus V_2\). We observe that if \(C\) is the convex cone of the set \rrh{\(X\subseteq U\)} and \(X = X_1 \cup X_2\), \rrh{with} \(X_1 \subseteq V_1\) and \(X_2 \subseteq V_2\), then \(v\) is an element of \(C\) if and only if there exist vectors \(v_1 \in V_1\) and \(v_2 \in V_2\) such that \(v = v_1 + v_2\) and \(v_1\) and \(v_2\) are in the convex cones of \(X_1\) and \(X_2\) respectively. Thus the computational complexity of determining whether \(v \in C\) is bounded by the complexity of determining whether the projections of \(v\) onto \(V_1\) and \(V_2\) are in their respective cones.

Since the worst case complexity is exponential in the dimension of \(V\) this is a significant improvement. Suppose the dimensions of \rrh{\(V_1\)} and \(V_2\) \rrh{are both \(10\), so that the dimension of \(V\) is \(20\)}. We see that \(2^{20}\) is far greater than \(2^{10} + 2^{10}\). 

\rrh{We can use the block structure (\cite[p.~244]{merrisbook}) of the decomposition matrix to obtain a cone with a decomposition as just described, namely, the cone corresponding to only those inequalities that involve irreducible characters in the same block.  Although this cone is not as large as the one obtained by using the full list of inequalities, it provides a natural way of getting results in some situations where the computations are prohibitively time consuming otherwise (cf.~Appendix \ref{chptr:GeneratedResults}).} \rrhnote{Your wording didn't exactly sell this idea, so I'm suggesting a different slant.  Rewrite if you wish.}

Consider the example of \(S_7\) with the prime \(3\). The decomposition matrix, when written in block form, is the following.

\[\begin{array}{c|ccccc|cc|cc}
&%
\rot{\bc{7}} & %
\rot{\bc{5,2}} & %
\rot{\bc{4,3}} & %
\rot{\bc{4,2,1}} & %
\rot{\bc{3,2,1^2}} & %
\rot{\bc{6,1}} & %
\rot{\bc{3,2^2}} & %
\rot{\bc{5,1^2}} & %
\rot{\bc{3^2,1}} \\
%
\oc{7}       & 1 & 0 & 0 & 0 & 0 & & & &\\
\oc{5,2}     & 1 & 1 & 0 & 0 & 0 & & & &\\
\oc{4,3}     & 0 & 1 & 1 & 0 & 0 & & & &\\
\oc{4,2,1}   & 1 & 1 & 1 & 1 & 0 & & & &\\
\oc{3,2,1^2} & 1 & 0 & 1 & 1 & 1 & & & &\\
\oc{4,1^3}   & 0 & 0 & 0 & 1 & 0 & & & &\\
\oc{2^3,1}   & 1 & 0 & 0 & 0 & 1 & & & &\\
\oc{2^2,1^3} & 0 & 0 & 1 & 0 & 1 & & & &\\
\oc{1^7}     & 0 & 0 & 1 & 0 & 0 & & & &\\
%
\oc{6,1}   & & & & & & 1 & 0 & &\\
\oc{3,2^2} & & & & & & 1 & 1 & &\\
\oc{3,1^4} & & & & & & 0 & 1 & &\\
%
\oc{5,1^2} & & & & & & & & 1 & 0\\
\oc{3^2,1} & & & & & & & & 1 & 1\\
\oc{2,1^5} & & & & & & & & 0 & 1\\
\end{array}\]
We \rrh{organize the} irreducible characters into their respective blocks and remove all inter-block relationships. For an example of the resulting partial order see Figure \ref{fig:pate7block}.
\begin{figure}%
\begin{center}\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={rectangle}]
    \node (n7) at (0,11) {\(\oc{7}\)};
    \node (n61) at (0,9) {\(\oc{6,1}\)};
    \node (n511) at (0,7) {\(\oc{5,1^2}\)};
    \node (n4111) at (1,5) {\(\oc{4,1^3}\)};
    \node (n31111) at (1,3) {\(\oc{3,1^4}\)};
    \node (n211111) at (1,1) {\(\oc{2,1^5}\)};
    \node (n1111111) at (1,-1) {\(\oc{1^7}\)};
    \node (n52) at (3,9) {\(\oc{5,2}\)};
    \node (n421) at (3,7) {\(\oc{4,2,1}\)};
    \node (n43) at (6,9) {\(\oc{4,3}\)};
    \node (n331) at (6,7) {\(\oc{3^2,1}\)};
    \node (n322) at (9,7) {\(\oc{3,2^2}\)};
    \node (n3211) at (5,5) {\(\oc{3,2,1^2}\)};
    \node (n2221) at (9,5) {\(\oc{2^3,1}\)};
    \node (n22111) at (5,3) {\(\oc{2^2,1^3}\)};
   \foreach \from/\to in  {n7/n61,n61/n511,n52/n421,n52/n511,n43/n331,%
                           n43/n421,n511/n4111,n421/n3211,n421/n4111,%
                           n331/n3211,n322/n2221,n322/n3211,n3211/n22111,%
                           n3211/n31111,n4111/n31111,n2221/n22111,%
                           n31111/n211111,n22111/n211111,n211111/n1111111}
    \draw (\from) -- (\to);

\end{tikzpicture}%
\caption{Pate's theorem on \(S_7\)}%
\label{fig:pate7}%
\end{center}\end{figure}%
\begin{figure}%
\begin{center}\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={rectangle}]
    \node (b1) at (13,0) {Block 1};
    \node (n7) at (9,7) {\(\oc{7}\)};
    \node (n52) at (11,11) {\(\oc{5,2}\)};
    \node (n43) at (13,11) {\(\oc{4,3}\)};
    \node (n421) at (11,9) {\(\oc{4,2,1}\)};
    \node (n3211) at (13,7) {\(\oc{3,2,1^2}\)};
    \node (n4111) at (11,5) {\(\oc{4,1^3}\)};
    \node (n2221) at (17,7) {\(\oc{2^3,1}\)};
    \node (n22111) at (15,5) {\(\oc{2^2,1^3}\)};
    \node (n1111111) at (13,3) {\(\oc{1^7}\)};

    \node (b2) at (3,0) {Block 2};
    \node (n61) at (1,5) {\(\oc{6,1}\)};
    \node (n322) at (5,5) {\(\oc{3,2^2}\)};
    \node (n31111) at (3,3) {\(\oc{3,1^4}\)};

    \node (b3) at (26,0) {Block 3};
    \node (n511) at (28,5) {\(\oc{5,1^2}\)};
    \node (n331) at (24,5) {\(\oc{3^2,1}\)};
    \node (n211111) at (26,3) {\(\oc{2,1^5}\)};
   \foreach \from/\to in  {n7/n4111,n61/n31111,n52/n421,%
                           n43/n421,n511/n211111,n421/n3211,n421/n4111,%
                           n331/n211111,n322/n31111,n3211/n22111,%
                           n4111/n1111111,n2221/n22111,n22111/n1111111}
    \draw (\from) -- (\to);

\end{tikzpicture}%
\caption{Pate's theorem on \(S_7\), split into blocks.}%
\label{fig:pate7block}%
\end{center}\end{figure}%


\input{../top_postamble}
