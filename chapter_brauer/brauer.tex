\input{../top_preamble}
\chapter{Brauer Characters}


Let \(\mathbb{O}\) be the ring of algebraic integers in \(\C\) and let \(p\) be a prime. The field \(\F\) is constructed by choosing a maximal ideal \(M\) \rrh{of \(\mathbb O\)} containing \(p\) and setting \(\F = \mathbb{O} / M\).\\
Denote by \(U\) the group of roots of unity of order relatively prime to \(p\) in \(\mathbb{O}\), and denote by \(U'\) the group of roots of unity in \(\F\).

\triminput{../thm_U_iso/thm_U_iso}

A \textbf{\(p\)-regular} member of a group \(G\) is an element of order relatively prime to \(p\). We denote by \labelx{notation:G'}\(G'\) the set of \(p\)-regular elements of \(G\). Group elements that are not \(p\)-regular are called \textbf{\(p\)-singular}. A \textbf{matrix representation} of \(G\) over a field \(\F\) is a homomorphism from \(G\) into \(\GL_m(\F)\) for some \(m\).

Let \(\rho:G \rightarrow \GL_m(\F)\) be a matrix representation of \(G\). If \(\sigma \in G'\), then the eigenvalues of \(\rho(\sigma)\) are all roots of unity (since \(\rho(\sigma)^k=I_n\) for some \(k\), every eigenvalue \(\lambda\) satisfies \(\lambda^k=1\)).

Given \(\rho\), define the \textbf{Brauer character} \(\phi\) corresponding to \(\rho\) by
\[\phi: G' \rightarrow \C,\]
\[\phi(\sigma) = \sum_{i=1}^m\iota(u_i),\]
where \(\{u_i\}_{i=1}^m\) are the eigenvalues of \(\rho(\sigma)\) counting multiplicity and \(\iota\) is the inverse of \(\pi|_U\).\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A Brauer character that cannot be written as the sum of other \rrh{B}rauer characters is called \textbf{irreducible}. The set of irreducible Brauer characters of \(G\) is denoted \labelx{notation:IBr}\(\IBr G\).
Given the above definition, it is natural to consider Brauer character analogues to Schur's theorem (\ref{thm:schur}) and the permanental dominance conjecture (\ref{conj:pd}).

\begin{conjecture}
\label{conj:naiveschur}
Let \(G \leq S_n\) and let \(\phi \in \IBr(G)\). Then \(\phi \geq \varepsilon\).
\end{conjecture}
\begin{conjecture}
\label{conj:naivepd}
Let \(G \leq S_n\) and let \(\phi \in \IBr(G)\). Then \(\phi \leq 1\).
\end{conjecture}
However, neither of these hold. We will show this using the group \(S_3\) and the prime 3.%
\begin{table}[h!]%
\begin{center}\begin{tabular}{ccc}%
\raisebox{-\height}{\(\begin{array}{r|rrr}%
&(.)&(..)&(...) \\
\hline
\oc{3}     & 1 &  1 &  1\\
\oc{2,1}   & 2 &  0 & -1\\
\oc{1^3} & 1 & -1 &  1
\end{array}\)} & & %
\raisebox{-\height}{\(\begin{array}{r|rr}
& (.) & (..) \\
\hline
\phi_1   & 1   & 1 \\
\phi_2   & 1   & -1
\end{array}\)}%
\end{tabular}%
\caption{Character tables of \(S_3\). The ordinary irreducible characters are on the left and the irreducible Brauer characters (for \(p=3\)) are on the right. Since characters are constant on conjugacy classes, these tables may be used to evaluate the corresponding functions. Elements of the symmetric group are conjugate when they have the same cycle structure, so we use these structures to denote the classes.}%
\end{center}\end{table}%

\begin{theorem}%
Conjecture \ref{conj:naiveschur} is false.
\end{theorem}
\begin{proof}
Let \[A = \left[%
\begin{array}{ccc}
1&1&1\\
1&1&1\\
1&1&1
\end{array}
\right].\]
Then \(A\) is positive semidefinite, but \(\det(A) = 1-1-1-1+1+1 = 0\) and \(\bar{d}_{\phi_2}(A) = 1-1-1-1+0+0 = -2\). Thus \(\phi_2 \not \geq \varepsilon\).
\end{proof}
\begin{theorem}
Conjecture \ref{conj:naivepd} is false.
\end{theorem}
\begin{proof}
Let \(G\) be the following graph.
\[\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={circle,fill=red!15}]
  \node (n1) at (1,1)  {1};
  \node (n2) at (1,3)  {2};
  \node (n3) at (3,2)  {3};
  \foreach \from/\to in {n1/n2,n1/n3,n2/n3}
    \draw (\from) -- (\to);
\end{tikzpicture}\]
Then the laplacian matrix \(L(G)\) is equal to \[\left[%
\begin{array}{rrr}
2 & -1 & -1 \\
-1 & 2 & -1 \\
-1 & -1 & 2
\end{array}
\right].\]
According to Theorem \ref{cor:laplacianpsd}, \(L(G)\) is positive semidefinite, but \(\per(L(G)) = 8+2+2+2-1-1 = 12\) and \(\bar{d}_{\phi_1}(L(G)) = 8+2+2+2+0+0 = 14\). Thus \(\phi_1 \not \leq 1\).
\end{proof}

%Brauer character names : \cite[6.3.59]{jamesandkerber}
%p-regular classes -- p-regular partitions \cite[6.1.2]{jamesandkerber}
%Partition-character correspondence: \cite[2.3.15]{jamesandkerber}

\triminput{../nice_matrices/nice_matrices}


If we specialize Conjecture \ref{conj:schurforibr} to the prime 2, we see that it holds in a trivial way.

\begin{theorem}
Let \(\pmat = \pmat_n(2)\) for some positive integer \(n\) and let \(f, g \in \C S_n\). Then \[f \leq_\pmat g.\]
\end{theorem}
\begin{proof}
By Theorem \ref{thm:p2}, \(\pmat_n(2)\) is a set consisting of diagonal matrices and matrices with a zero row and column.

For a diagonal matrix \(D\), %
\[d_f(D) = %
\sum_{\sigma \in S_n} f(\sigma) \prod_{i=1}^n d_{i\sigma(i)} = %
f(\e) \prod_{i=1}^n d_{ii},\]
so we have %
\[g(\e)d_f(D) = g(\e)f(\e)\prod_{i=1}^n d_{ii} = f(\e)d_g(D).\]
For a matrix \(C\) with a zero row or column, \(d_f(C) = 0\), so \rrh{once} again \(g(\e)d_f(C) = f(\e)d_g(C)\). Thus we have \(f \leq_\pmat g\) as desired.
\end{proof}

Recall the role of Watkins' theorem (\ref{thm:watkins}) in the proof of Schur's theorem (\ref{thm:schur}). We suspect a similar approach may work to prove Conjecture \ref{conj:schurforibr}, but both the hypothesis and conclusion of Watkins' theorem are too strong for our needs. This leads us to the following alternative.

\triminput{../chapter_brauer/watkinsnice.tex}

\begin{table}[b]%
\begin{center}\[\begin{array}{r|ccccc}
S_5, p=3&\rot{\bc{5}}&\rot{\bc{4,1}}&\rot{\bc{3,2}}&\rot{\bc{3,1^{2}}}&\rot{\bc{2^{2},1}}\\
\hline
\oc{5}&1&0&0&0&0\\
\oc{4,1}&0&1&0&0&0\\
\oc{3,2}&0&1&1&0&0\\
\oc{3,1^{2}}&0&0&0&1&0\\
\oc{2^{2},1}&1&0&0&0&1\\
\oc{2,1^{3}}&0&0&0&0&1\\
\oc{1^{5}}&0&0&1&0&0
\end{array}\]\end{center}%
\caption{The decomposition matrix for \(S_5\) relative to the prime 3.}%
\label{table:decompS5P3}%
\end{table}%


The \textbf{decomposition matrix} \rrh{of the group \(G\) with respect to the prime \(p\)} is the matrix representation of the restriction map from span\((\Irr(G))\) to span\((\IBr(G))\) (with respect to the bases \(\Irr(G)\) and \(\IBr(G)\)). A rather large collection of decomposition matrices is contained in \rrh{the computer algebra system} GAP \cite{gap}.

Each row \rrh{of the decomposition matrix} tells us how to express the restriction of an ordinary character as a sum of Brauer characters. \rrh{More precisely}, if \rrh{\(D = (d_{\chi\phi})\) is} the decompositiom matrix\rrh{,} then for each \(\chi \in \Irr(G)\) we have \(\chi'=\sum_{\phi\in\IBr(G)} d_{\chi\phi}\phi\), where \(\chi'\) is the restriction of \(\chi\) to \(p\)-regular elements of \(G\)\rrh{\ }\cite[p.~267]{isaacs}.

We call the partitions of \(n\) that do not contain \(p\) copies of the same integer \textbf{\(p\)-regular}. In an approach similar to \cite{jamesandkerber} we will use the decomposition matrix to assign \(p\)-regular partitions as labels to \rrh{the elements of} \(\IBr (S_n)\).  Recall that there is a one-to-one correspondence between the set of partitions \(\alpha\) of \(n\) and the irreducible characters \(\oc{\alpha} \in \Irr (S_n)\). The row of the decomposition matrix corresponding to the irreducible character \oc{\alpha} is called row \(\alpha\).

%Using this and the one-to-one correspondence between the set of partitions of \(n\) and the set \(\Irr{S_n}\) %
%\cite[52]{jamesandkerber}, we can set up the following naming scheme. Given \rrh{a partition} \(\pi\) \rrh{of \(n\)}, \oc{\pi} will denote the corresponding ordinary irreducible character of \(S_n\). To assign names to Brauer characters we use the same approach as in \cite{jamesandkerber}, made possible by the following theorem.\labelx{notation:oc}

\begin{theorem}[{\cite[p.~282]{jamesandkerber}}]
\label{thm:columnnames}
Arrange the rows of the decomposition matrix in lexicographic order, and let \(\alpha\) be a \(p\)-regular partition of \(n\). Then there is a column of the decomposition matrix whose first nonzero entry is on row \(\alpha\), and this entry is a 1. Additionally, given a \(p\)-regular partition \(\beta\), the entry on row \(\beta\) of this column \rrh{is} nonzero only if \(\alpha\) majorizes \(\beta\).
\end{theorem}
Theorem \ref{thm:columnnames} gives us a way to assign a Brauer character to each \(p\)-regular partition \(\alpha\) of \(n\). For every such partition we call the column corresponding to row \(\alpha\) in Theorem \ref{thm:columnnames} column \(\alpha\) and denote the associated Brauer character by \labelx{notation:bc}\bc{\alpha}. Since the number of \(p\)-regular partitions of \(n\) is equal to \(|\IBr(S_n)|\) \cite[285]{jamesandkerber}, this assigns a label to every irreducible Brauer character of \(S_n\).



Observe that the decomposition matrix in Table \ref{table:decompS5P3} is a row permutation of a matrix of the form \scalemath{0.5}{\left[\begin{array}{c}I \\ \hline X\end{array}\right]}, where \(I\) is the identity matrix. This means that each irreducible Brauer character is the image of an ordinary irreducible character under the restriction map.
\begin{theorem}
\label{thm:characterrestrictions}
Let \rrh{\(p = 3\)}. If \(n \leq 5\) and \(\phi\) is an irreducible Brauer character of \(S_n\), then
\[\phi \geq_\pmat \varepsilon.\]
\end{theorem}
\begin{proof}
By inspecting the decomposition matrices for \(S_n\) and the prime 3, we can see that all of the Brauer characters are just restrictions of ordinary characters of \(S_n\). For \(\chi \in \Irr(S_n)\) and \(A \in \pmat\), we have %
\[\bar{d}_{\chi'}(A) = \bar{d}_{\chi}(A) \geq \bar{d}_{\varepsilon}(A),\]
\rrh{where the inequality is from Theorem \ref{thm:schur}.}
\end{proof}%
\begin{center}\begin{table}%
\[\begin{array}{r|ccccccc}
S_6, p=3&\rot{\bc{6}}&\rot{\bc{5,1}}&\rot{\bc{4,2}}&\rot{\bc{3^{2}}}&\rot{\bc{4,1^{2}}}&\rot{\bc{3,2,1}}&\rot{\bc{2^{2},1^{2}}}\\
\hline
\oc{6}&1&0&0&0&0&0&0\\
\oc{5,1}&1&1&0&0&0&0&0\\
\oc{4,2}&0&0&1&0&0&0&0\\
\oc{3^{2}}&0&1&0&1&0&0&0\\
\oc{4,1^{2}}&0&1&0&0&1&0&0\\
\oc{3,2,1}&1&1&0&1&1&1&0\\
\oc{2^{2},1^{2}}&0&0&0&0&0&0&1\\
\oc{2^{3}}&1&0&0&0&0&1&0\\
\oc{3,1^{3}}&0&0&0&0&1&1&0\\
\oc{2,1^{4}}&0&0&0&1&0&1&0\\
\oc{1^{6}}&0&0&0&1&0&0&0
\end{array}\]%
\caption{The decomposition matrix for \(S_6\) relative to the prime 3.}%
\label{table:decompS6P3}%
\end{table}\end{center}

For \(S_6\) and the prime 3 there exist irreducible Brauer characters that are not restrictions of ordinary irreducibles. By inspecting the decomposition matrix (Table \ref{table:decompS6P3}) we may see that they are \bc{5,1}, \bc{4,1^2}, and \bc{3,2,1}.

However, \bc{3,2,1} is a difference of restricted ordinary characters:%
\[\bc{3,2,1} = \ocr{2,1^4} - \ocr{1^6}.\]%
We will be able to use this fact to ``push'' inequalities of ordinary irreducible characters through the decomposition map to generate an inequality involving \bc{3,2,1}.

\begin{theorem}[Pate's Theorem {\cite[p225]{merrisbook}}]
\label{thm:pate}%
Suppose \(\pi = [\pi_1,\pi_2,\dots,\pi_t]\) and \[\rho = [\pi_1, \pi_2, \dots,\pi_{s-1}, \pi_s -1, \pi_{s+1}, \dots, \pi_t,1]\] are partitions of \rrh{\(n\)}. Then \(\pi \geq \rho\).
\end{theorem}

\begin{corollary}% \ocr{2,1^4} - \ocr{1^6}
\[\bc{3,2,1} \geq_{\pmat_6(3)} \varepsilon.\]
\end{corollary}
\begin{proof}
From Pate's theorem, we know \(\oc{2,\rrh{1^4}} \geq \oc{1^6}\), so
\[ \frac{\oc{2,1^4}}{\oc{2,1^4}(\e)} \succeq \frac{\oc{1^6}}{\oc{1^6}(\e)} .\]
Using the hook formula (see Theorem \ref{thm:hook} and explanation), we compute the degrees of the characters to be \(\oc{2,1^4}(\e) = 5\) and \(\oc{\rrh{1^6}}(\e) = 1\). We have
\[ \oc{2,1^4} - 5\oc{1^6} \succeq 0,\]
\[ \ocr{2,1^4} - 5\ocr{1^6} \succeq_\pmat 0,\]
\[ \bc{3,2,1} = %
\ocr{2,1^4}-\ocr{1^6} %
\succeq_\pmat %
\ocr{2,1^4} - 5\ocr{1^6} \succeq_\pmat 0.\]
Since \(\bc{3,2,1}\) is nonnegative on \nice{p} matrices, the conclusion follows from Theorem \ref{thm:watkinsnice}.%
\end{proof}
There are two other Brauer characters of \(S_6\) with respect to the prime 3 that are not restrictions of ordinary irreducible characters. We can prove a similar inequality for them using the same strategy. The application to \bc{4,1^2} is pretty straightforward, but \bc{5,1} will be more involved.

\begin{corollary}
\[\bc{4,1^2} \geq_\pmat \varepsilon.\]
\end{corollary}
\begin{proof}
Using Table \ref{table:decompS6P3}, we can see that \(\bc{4,1^2} = \ocr{3,1^3} + \ocr{1^6} - \ocr{2,1^4}.\) Pate's theorem gives us \(\oc{3,1^3} \geq \oc{2,1^4}\), which implies \(\oc{3,1^3} \succeq 2\oc{2,1^4}\), and hence \({\ocr{3,1^3}}-2{\ocr{2,1^4}}\succeq_\pmat 0\). Using this, we can split \(\bc{4,1^2}\) \rrh{into} a sum of three parts, each of which is nonnegative on matrices in \(\pmat\).
\[{\bc{4,1^2}} = \frac{1}{2}{\ocr{3,1^3}} + {\ocr{1^6}} + \frac{1}{2}\left(\ocr{3,1^3} - 2\ocr{2,1^4}\right).\]
Again, by Theorem \ref{thm:watkinsnice}, \(\bc{4,1^2} \geq_\pmat \varepsilon\).
\end{proof}
\begin{figure}%
\begin{center}\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={rectangle}]
  \node (n111111) at (1,1) {\(\oc{1^6}_1\)};
  \node (n21111)  at (1,3)  {\(\oc{2,1^4}_5\)};
  \node (n3111)   at (1,5)  {\(\oc{3,1^3}_{10}\)};
  \node (n2211)   at (4,5)  {\(\oc{2^2,1^2}_9\)};
  \node (n411)    at (1,7)  {\(\oc{4,1^2}_{10}\)};
  \node (n321)    at (4,7)  {\(\oc{3,2,1}_{16}\)};
  \node (n222)    at (7,7)  {\(\oc{2^3}_5\)};
  \node (n51)     at (1,9)  {\(\oc{5,1}_5\)};
  \node (n42)     at (4,9)  {\(\oc{4,2}_9\)};
  \node (n33)     at (7,9)  {\(\oc{3^2}_5\)};
  \node (n6)      at (1,11)  {\(\oc{6}_1\)};
  \foreach \from/\to in {n111111/n21111%
                        ,n3111/n411,n3111/n321,n2211/n222%
                        ,n411/n51,n411/n42,n321/n42,n51/n6}
    \draw (\from) -- (\to);
  \foreach \from/\to in {n2211/n321,n321/n33,n21111/n3111,n21111/n2211}
    \draw %[ line width=1.2pt]
      (\from) -- (\to);
\end{tikzpicture}%
\caption{Pate's theorem on \(S_6\). The subscripts are the character degrees.}%
\label{fig:pate6}%
\end{center}\end{figure}%

This approach so far can be summarized as \rrh{follows:}%
\begin{enumerate}
\item Assemble a pool of inequalities of ordinary characters\rrh{.}
\item \rrh{F}ind the corresponding inequalities of Brauer characters\rrh{.}
\item \rrh{F}ind a way to compose those inequalities to prove the desired Brauer character \(\phi\) satisfies \(\phi \succeq_\pmat 0\).%
\item Conclude from Theorem \ref{thm:watkinsnice} that \(\phi \geq_\pmat \varepsilon\).
\end{enumerate}
The group \(S_6\) has 11 ordinary irreducible characters, and Figure \ref{fig:pate6} shows the partial order revealed by Pate's theorem. For each of the 12 edges we have a \(\geq\)-inequality of irreducible characters and a corresponding \(\succeq\)-inequality. This, via the decomposition map, yields an inequality of Brauer characters. 

\triminput{../chapter_brauer/decomp_table}

In addition to the inequalities in Table \ref{tbl:pate63}, we have a set of 11 similar inequalities generated by Schur's theorem. For example, consider \(\oc{3,2,1} \in \Irr (S_6)\). We have%
\begin{align*}
\oc{3,2,1} &\succeq 0, \\
\bc{6} + \bc{5,1} + \bc{3^{2}} + &\bc{4,1^{2}} + \bc{3,2,1} \succeq_\pmat 0 \tag{5}.
\end{align*}
After some exploration we eventually discover that a linear combination of the inequalities (1), (2), (3), (4), and (5) (with coefficients \(5\), \(1\), \(1\), \(1\), and \(\frac{1}{4}\) respectively) yields the inequality \(\bc{5,1} \succeq_\pmat 0\) (and thus \(\bc{5,1} \geq_\pmat \varepsilon\)). Since the corresponding inequalities of ordinary characters are shorter, we may use our knowledge of these coefficients to construct a less cluttered proof by postponing the application of the decomposition map.
\begin{corollary}
\[\bc{5,1} \geq_\pmat \varepsilon\]%
\label{corr:51}%
\end{corollary}
\begin{proof}
For each of (1), (2), (3), and (4) we multiply the corresponding inequalities in the middle column of Table \ref{tbl:pate63} by 5, 1, 1, and 1, respectively. We have
\begin{align*}
\oc{3^{2}} - \frac{5}{16}\oc{3,2,1} &\succeq 0, \\
\frac{1}{16}\oc{3,2,1} - \frac{1}{9}\oc{2^{2},1^{2}} &\succeq 0, \\
\frac{1}{9}\oc{2^{2},1^{2}} - \frac{1}{5}\oc{2,1^{4}} &\succeq 0, \\
\frac{1}{5}\oc{2,1^{4}} - \oc{1^{6}} &\succeq 0,
\end{align*}
and adding these inequalities up yields
\[\oc{3^2}-\oc{1^6}-\frac{1}{4}\oc{3,2,1}\succeq 0.\]
Thus,
\[\bc{5,1} = \ocr{3^2}-\ocr{1^6} \succeq_\pmat \frac{1}{4}\ocr{3,2,1} \succeq_\pmat 0,\]
where the equality is from Table \ref{table:decompS6P3}. We then have \(\bc{5,1} \geq_\pmat \varepsilon\) by Theorem \ref{thm:watkinsnice}.%
\end{proof}
However, this proof has a noticable shortcoming. In order to produce it we had to somehow discover the ``right coefficients'' with which to combine the available inequalities and reach the desired conclusion, but no process for obtaining them has been provided. Unsatisfied with our dependence on sudden flashes of inspiration, we will proceed to discuss a reproducible technique by which a set of ``right coefficients'' can be determined.
\input{../top_postamble}



