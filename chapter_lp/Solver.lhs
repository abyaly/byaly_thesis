\input{../top_preamble}
\section{Producing a solution}
As before, this section is a literate Haskell program. It may be built by \ctt{ghc Solver.lhs}. Here we assemble all of the previous work and run it through an external linear program solver to answer the question of cone membership.
\begin{code}
module Solver where
import Decomposition
import ConstructLP
import Appendix
\end{code}
We assume the solver is in the \ctt{lp} directory, and that it accepts the description of a linear program in MPS 
fixed column format. Fixed MPS format\cite{mps} was originally used for linear programming on mainframe systems in 
the 1960s and has since become an industry standard. We further assume that the solver produces a Haskell 
expression representing \rrh{a} value of type \ctt{Either [Scalar] [Scalar]} and sends it to standard output. The 
\ctt{Right} values will \rrh{be} used for solutions and the \ctt{Left} values will be used for Farkas certificates. 
\begin{code}
solver = "./lp/solver.sh"
\end{code}
The search function combines all of our previous work. Given a positive integers \(n\), \(k\) and a prime \(p\), 
we generate a cone of inequalities of Brauer characters of \(S_n\) with respect to \(p\). Then we set up the 
linear program for determining whether the \rrh{\(k\)th} irreducible Brauer character (in the order we used for our 
basis) is a member of the cone. 
\begin{code}
search :: (Natural n, Natural p) =>
          n -> p -> Int -> IO ( Either [Scalar] [Scalar] )
search n p k = do
  d <- gap_decomposition_matrix n p
  let cols = cone d
  let target = (embed' . (@@k)) (ibrBasis n p)
  let mps = mps_lp cols target --The details of expressing a linear program
                               --in MPS format will come later.
\end{code}
Next, we record the linear program (in case we want to inspect it later) and dispatch our linear program solver. 
\begin{code}
  let mpsfile = concat ["./lp/",show n,"/",show p,"/",show k,".mps"]
  writeFile mpsfile mps
  result <- readProcess solver [] mps
\end{code}
The result should be either a solution or a Farkas certificate, and in either case we check that it has the correct algebraic property. 
\begin{code}
  let resultFile = concat ["./lp/",show n,"/",show p,"/",show k,".out"]  
  let solution = read result :: Either [Scalar] [Scalar]
  let integrity = verify cols target solution
  if integrity then writeFile resultFile result
               else error "Something has gone terribly wrong."
  return solution
\end{code}
The MPS fixed format requires us to name the rows and columns of our linear system. We will just name them after the order in which they appear.
\begin{code}
rowname :: Int -> String
rowname i = ("R" ++ show i)
colname :: Int -> String
colname i = ("X" ++ show i)
\end{code}
A data file in MPS fixed format represents a deck of computer input cards, with each row corresponding to a card. Each row is divided into 6 fields, determined by the character position. The positions of the first characters in each field are 1, 5, 15, 25, 40, and 50 respectively. The file is divided into sections delimited by rows with the section indicator in field 1. The meaning of the data in other fields is determined by the section the row is in.
\begin{code}
mps_lp :: (Natural n, Natural p) => [VecCSn' n p] -> VecCSn' n p -> String
mps_lp columns rhs = concat 
  [ mps_section_name    columns rhs
  , mps_section_rows    columns rhs
  , mps_section_columns columns rhs
  , mps_section_rhs     columns rhs
  , "ENDATA\n"  ]
\end{code}
In order to correctly fit our data into the MPS fixed format we will be using these utility functions to append whitespace to either the left or right side.
\begin{code}
padRight :: Int -> String -> String
padRight x = take x . (++spaces)
  where 
  spaces = ' ':spaces
padLeft :: Int -> String -> String
padLeft x s = (reverse . take x . (++spaces) . reverse) s
  where 
  spaces = ' ':spaces
\end{code}
The (optional) name section consists of just the indicator \ctt{NAME} in field 1 and the name of the problem in field 3.
\begin{code}
mps_section_name :: (Natural n, Natural p) => 
  [VecCSn' n p] -> VecCSn' n p -> String
mps_section_name _ v = concat 
  [ padRight 14 "NAME"
  , "S", (show . value . getN . getSpace) v
  , "P", (show . value . getP . getSpace) v
  , "\n"  ] 
\end{code}
In the rows section field 1 is used to indicate the type of inequality used (we always use \ctt{E} for equality 
\rrh{constraints}, but \ctt{G} and \ctt{L} may be used to indicate \(\geq\) and \(\leq\) respectively), and field 2 
indicates the name of the corresponding row. 
\begin{code}
mps_section_rows :: (Natural n, Natural p) => 
  [VecCSn' n p] -> VecCSn' n p -> String
mps_section_rows _ (Vec coeffs) = "ROWS\n" ++ concatMap f [1..d]
  where
  f x = " E  " ++ rowname x ++ "\n"
  d = length coeffs
\end{code}
In the \ctt{COLUMNS} section, the fields have the following meanings.
\begin{center}\begin{tabular}{|c|l|}
\hline
Field 1 & Blank.\\
\hline
Field 2 & Column name.\\
\hline
Field 3 & Row name.\\
\hline
Field 4 & The value of the coefficient in the position specified by field 2 and field 3.\\
\hline
Field 5 & (Optional) row name.\\
\hline
Field 6 & (Optional) value of the coefficient in the position specified by field 2 and field 5.\\
\hline
\end{tabular}\end{center}

\bigskip
\noindent For example, in the linear system%
\begin{equation}%
\label{eq:examplemps}%
\left[\begin{array}{ccc}
  1 & 0 & 2 \\
  2 & 1 & 0 \\
  1 & 3 & 0
  \end{array}\right]%
  x = \left[\begin{array}{c} 5 \\ 0 \\ 2 \end{array}\right],%
\end{equation}%
if we give the rows and columns 
\rrh{the} names \ctt{R1}, \ctt{R2}, \ctt{R3} and \ctt{C1}, \ctt{C2}, \ctt{C3} respectively the \ctt{COLUMNS} section will be as follows.
\begin{verbatim}
COLUMNS
    C1        R1                   1   R2                  2
    C1        R3                   1
    C2        R2                   1   R3                  3
    C3        R1                   2
\end{verbatim}
We omit the entries corresponding to zeroes since every coefficient we do not specify will be assumed to be zero.

Here we create the ordered pairs necessary to fill the columns section, but the work of organizing the data into the correct format is in the implementation of the \ctt{column} function.
\begin{code}
mps_section_columns :: (Natural n, Natural p) => 
  [VecCSn' n p] -> VecCSn' n p -> String
mps_section_columns cols v = 
  "COLUMNS\n" ++ concat (zipWith format [1..] cols)
  where
  format col (Vec coeffs) = 
    column (colname col)
    [ (rowname row, c) |  (row,c) <- zip [1..] coeffs, c /= 0]
\end{code}
The \ctt{column} function expects a column name and a list of ordered pairs of the form (row name, coefficient), and it generates the corresponding rows of the MPS record.
\begin{code}
column :: String -> [(String,Scalar)] -> String
column col [] = ""
column col [(r,c)] = concat 
      [ "    ", padRight 8 col
      ,   "  ", padRight 8 r
      ,   "  ", (padLeft 12 . show . floor) c
      ,   "\n"]
column col ( (r1,c1):(r2,c2):rest ) = concat
      [ "    ", padRight 8 col
      ,   "  ", padRight 8 r1
      ,   "  ", (padLeft 12 . show . floor) c1
      ,  "   ", padRight 8 r2 
      ,   "  ", (padLeft 12 . show . floor) c2
      ,   "\n"] ++ column col rest
\end{code}
The \ctt{RHS} section specifies the coefficients of the right hand side. The format is the same as the \ctt{COLUMNS} section. The \ctt{RHS} section corresponding to equation (\ref{eq:examplemps}) is the following.
\begin{verbatim}
RHS
    rhs        R1                   5   R3                  2
\end{verbatim}
To populate it we rely on the previously defined \ctt{column} function.
\begin{code}
mps_section_rhs :: [VecCSn' n p] -> VecCSn' n p -> String
mps_section_rhs _ (Vec rhs) = "RHS\n" ++ 
  column "rhs" [ (rowname row, c) |  (row,c) <- zip [1..] rhs, c /= 0]
\end{code}
Once we have retrieved an answer from the linear program solver, we make sure it has the desired algebraic property. We expect a \ctt{Right} value to be a nonnegative solution to the provided linear system.
\begin{code}
verify :: [VecCSn' n p] -> VecCSn' n p -> Either [Scalar] [Scalar] -> Bool
verify cols b (Right x) = 
  (all (>= 0) x) && (sumv (zipWith (*^) x cols) == b)
  where 
  sumv = foldr (^+^) zero
  zero = Vec (repeat 0)
\end{code}
We expect a \ctt{Left} value to be a Farkas certificate. Geometrically, a Farkas certificate is the normal vector 
to a hyperplane separating the \rrh{vector on the} right hand side \rrh{of the equation defining the linear program} from the convex cone of the columns of the matrix. To verify that a 
given vector \(y\) is a Farkas certificate, we check that the scalar product of \(y\) with the right hand side 
\(b\) is nonzero and that for every column \(c\) we have  \(\operatorname{sig}(\langle c , y \rangle) \neq 
\operatorname{sig}(\langle b , y \rangle)\), where \[\operatorname{sig}(x) =% 
\left\lbrace%
\begin{array}{ccl}%
-1 & \text{if} & x < 0, \\
0 & \text{if} &  x = 0, \\
1 & \text{if} &  x > 0.
\end{array}\right.\]
This ensures that every column vector either lies on the hyperplane orthogonal to \(y\) or is on a different side of it from \(b\).
\begin{code}
verify cols b (Left y') = (s /= 0) &&
  (not . (any sameSide)) cols
  where
  y = Vec y'
  s = b ^*^ y
  sameSide c = signum (c ^*^ y) == s
\end{code}
\input{../top_postamble}
