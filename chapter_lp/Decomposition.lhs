\input{../top_preamble}
\section{The Decomposition Matrix}

In this section we will produce the decomposition matrix for a given symmetric group and prime by extracting it 
from \rrh{the computer algebra system} GAP \cite{gap}. The GAP system does not compute the decomposition matrices directly. Rather, it provides an 
interface to the Modular Atlas project \cite{moc} which aims to produce Brauer character tables for all groups in 
the ATLAS of finite groups \cite{conway1985atlas}. 

As with the previous section, this is a literate Haskell program. It may be built using the command \ctt{ghc Decomposition.lhs}.

\begin{code}
module Decomposition where
import ConstructLP
import Appendix
\end{code}

We begin by defining our interface to GAP. It is not necessary for us to capture all of GAP's functionality. It is sufficient to be able to execute a single command and extract the result. The following data type represents the GAP functions and values we will be using.
\begin{code}
data Gap = T  String           --A literal value with no arguments.
         | F1 String Gap       --A function with a single argument.
         | F2 String (Gap,Gap) --A two argument function in prefix notation.
         | I2 String Gap Gap   --An infix two-argument function.
         deriving (Show)
\end{code}
We then describe how to convert our internal representation of a GAP command into a string that GAP is able to use.
\begin{code}
render :: Gap -> String
render (T s) = s
render (F1 f x) = concat [f,"( ",render x," )"]
render (F2 f (x1,x2)) = concat [f,"( ",render x1," , ",render x2," )"]
render (I2 f x1 x2) = concat ["( ",render x1," ) ",f," ( ",render x2," )"]
\end{code}
The relationship between our internal GAP instructions and the ones we export is as follows.\\
\ctt{> render (T "val")}\\
\ctt{val}\\
\ctt{> render (F1 "foo" (T "val"))}\\
\ctt{foo( val )}\\
\ctt{> render (F2 "foo" (T "val1") (T "val2"))}\\
\ctt{foo( val1 , val2 )}\\
\ctt{> render (I2 "foo" (T "val1") (T "val2"))}\\
\ctt{( val1 ) foo ( val2 )}\\
Invoking GAP on the command line via \ctt{gap -q} runs a GAP session that accepts standard input and prints the results to standard output.
\begin{code}
run_gap :: Gap -> IO String
run_gap cmd = readProcess "gap" ["-q"] (render cmd ++ ";\nquit;") 
\end{code}
With this our interface to GAP is complete. The manual for the GAP character table library provides instructions for computing decomposition matrices \cite{gapdecomp}, which we implement here.
\begin{code}
sym :: (Natural n) => n -> Gap
sym n = T ("\"S" ++ (show . value) n ++ "\"")
-- ~\ctt{> render (sym 3)}~
-- ~\ctt{"S3"}~

(%) :: Gap -> Gap -> Gap
(%) = I2 "mod"
-- ~\ctt{> render (T "x" \% T "y")}~
-- ~\ctt{x mod y}~

gapmodtbl :: (Natural n, Natural p) => n -> p -> Gap
gapmodtbl n p = F1 "CharacterTable" (sym n) % (T . show) p
-- ~\ctt{> render ( gapmodtbl n6 n3 )}~
-- ~\ctt{CharacterTable( "S6" ) mod 3}~
\end{code}

\begin{code}
gap_decomposition_matrix :: (Natural n, Natural p) => 
  n -> p -> IO (DecompositionMatrix n p)
gap_decomposition_matrix n p = do
  gapout <- run_gap $  F1 "DecompositionMatrix" (gapmodtbl n p)
\end{code}
The output produced when asking GAP to display a decomposition matrix is identical to Haskell's list syntax.
\begin{verbatim}
gap> DecompositionMatrix( CharacterTable( "M11" ) mod 2 );
[ [ 1, 0, 0, 0, 0 ], [ 0, 1, 0, 0, 0 ], [ 0, 1, 0, 0, 0 ], 
  [ 0, 1, 0, 0, 0 ], [ 1, 1, 0, 0, 0 ], [ 0, 0, 1, 0, 0 ], 
  [ 0, 0, 0, 1, 0 ], [ 0, 0, 0, 0, 1 ], [ 1, 0, 0, 0, 1 ], 
  [ 1, 1, 0, 0, 1 ] ]
\end{verbatim}
Due to the similarity we can parse the result into a matrix of integers by using the existing read function.
\begin{code}
  let mat = (fmap.fmap) fromIntegral $ read gapout
\end{code}
The rows and columns of this matrix are naturally not in the same order as our
basis. To put them in order we need to ascertain which row corresponds
to which partition of \(n\), the details of which we defer for now.
\begin{code}
  labels <- gap_decomposition_row_labels n p
\end{code}
With this information we may then order the rows by comparing their partitions.
\begin{code}
  let sortRows rows = 
        (fmap snd . sortOn fst . (zip labels)) rows
\end{code}
However, we cannot order the columns in a similar way. Recall that we assign labels to the columns of the decomposition matrix according to theorem \ref{thm:columnnames}, which specifies that the rows are in lexicographic order. We could briefly reorder the rows to deduce the column names, but this is not required since the order they are in admits the same approach.
\begin{theorem}
Arrange the rows of the decomposition matrix so that
\begin{itemize}
\item rows corresponding to \(p\)-regular partitions come first,
\item and for every pair of \(p\)-regular partitions \(\alpha, \beta\) such that \(\alpha\) majorizes \(\beta\), row \(\alpha\) precedes row \(\beta\).
\end{itemize}
Then for every \(p\)-regular partition \(\pi\), the first nonzero entry of column \(\pi\) occurs on row \(\pi\).
\end{theorem}
\begin{proof}
Fix the column \(\pi\), and consider a partition \(\alpha\) such that row \(\alpha\) is above row \(\pi\). Then \(\alpha\) is \(p\)-regular and \(\pi\) does not majorize \(\alpha\). By theorem \ref{thm:columnnames}, the entry on row \(\alpha\) is zero. From the same theorem, we know that the entry on row \(\pi\) is 1, completing the proof.
\end{proof}
Since our current ordering meets the hypothesis of this theorem, each column shares a label with the row on which its first nonzero entry occurs. The ordering we have imposed on the row labels is the same for the column labels, so once the columns are sorted the decomposition matrix will be lower triangular. There is only one column order with this property. If we skip deducing the column labels and just rearrange the columns to create a lower triangular matrix, the result will have the columns in the correct order. We do this by sorting based on the index of the first nonzero entry.
\begin{code}
  let sortCols = transpose . sortOn (elemIndex 1) . transpose 
\end{code}
Then all we need to do is wrap the matrix in \ctt{LinearMap} to make the types match.
\begin{code}
  let mat' = (transpose . sortCols . sortRows) mat
  return (LinearMap mat')
\end{code}
The row labels are contained in the character parameters of the ordinary character table. They are partitions represented as descending lists of integers.
\begin{code}
gap_decomposition_row_labels :: (Natural n, Natural p) => 
  n -> p -> IO [Partition n p]
gap_decomposition_row_labels n p = do
    let tbl = F1 "OrdinaryCharacterTable" ( gapmodtbl n p )
        second g = F2 "List" (g, T "x -> x[2]")
    gapout <- (run_gap . second . F1 "CharacterParameters") tbl
\end{code}
Similarly to before, we exploit the fact that lists are represented the same way in GAP and in Haskell. The \ctt{read} command can already parse them without extra work on our part.%
\begin{code}
    let ps = map Partition . read $ gapout
    return ps
\end{code}%
\input{../top_postamble}
