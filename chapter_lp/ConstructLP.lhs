\input{../top_preamble}
\section{Setting up the linear program}
\label{section:constructlp}
We present an implementation of this technique. Given a positive integer \(n\), a prime \(p\), and a Brauer character \(\phi \in \IBr(S_n)\), we construct a linear program \(L\) such that if \(L\) has a solution then \(\phi \succeq_{\pmat_n(p)} 0\). This section is a literate Haskell program that can be built using the command \ctt{ghc ConstructLP.lhs}.
\begin{code}
module ConstructLP where
import Appendix
\end{code}
We will be indexing our space of functions using the corresponding partitions of \(n\). We begin by defining an element of the data type \ctt{Partition n p} to be the word ``Partition'' followed by a list of \ctt{Int}s.
\begin{code}
data Partition n p = Partition [Int] deriving (Eq, Show, Read)
list :: Partition n p -> [Int]
list (Partition xs) = xs
\end{code}
\begin{remark}
The type \ctt{Partition} is called a \textbf{phantom type} because it has type variables that do not appear in the right hand side of the definition. A value of type \ctt{Partition A B} is in a sense identical to a value of type \ctt{Partition C D}. Each of them is the word Partition followed by a list of integers. However we have specified that these are different types, so an attempt to use one in place of the other is a type error and will not compile. 
\end{remark}
We will use the type \ctt{n} on the left hand side to specify which natural number this is a partition of. Additionally, we need to order our basis so that we may write elements of our vector space as tuples. We will be using a different ordering depending on the prime chosen, so the type \ctt{p} on the left hand side will specify the prime in use. We may extract the \(n\) and \(p\) associated with a partition through the following functions.
\begin{code}
getN :: a n p -> n
getN = undefined
getP :: a n p -> p
getP = undefined
\end{code}
\begin{remark}
Since we \rrh{are} representing \(n\) and \(p\) using types rather than values, it \rrh{is not} necessary for our implementations of \ctt{getN} and \ctt{getP} to produce a value of the associated type. Suppose we have a value \ctt{t} of type \ctt{Partition N6 N3}. We may infer that the type of \ctt{getN t} is \ctt{N6}. The function \ctt{value :: N6 -> Integer} ignores the value of its argument and returns \ctt{6}, so \ctt{value (getN x)} returns \ctt{6}. \rrhnote{Above, you seem to be using regular mathematics typeface for values (like \(n\) and \(p\)).  Wouldn't it make more sense to have \(6\) here?} The implementation of this is in the \ctt{Appendix} module.
\end{remark}
We want to choose an ordering so that the decomposition matrix, once we have it, is as similar as possible to the tables of decomposition matrices in \cite{jamesandkerber}. To implement our ordering we make \ctt{Partition n p} an instance of the \ctt{Ord} type class by defining \ctt{compare :: Partition n p -> Partition n p -> Ordering}. The function \ctt{compare} is expected to return one of \ctt{LT}, \ctt{GT}, or \ctt{EQ}, depending on whether the first argument is less than, greater than, or equal to the second one. Our ordering will make p-regular partitions least.
\begin{code}
instance (Natural n, Natural p) => Ord (Partition n p) where
  compare t t' = case (isRegular t, isRegular t') of
    (True, False) -> LT
    (False, True) -> GT
\end{code}
Next, shorter partitions will be less than longer ones.
\begin{code}
    _ -> case compare ((length . list) t) ((length . list) t') of
      LT -> LT
      GT -> GT
\end{code}
And lastly, if everything else is equal and \(\alpha\) majorizes \(\beta\), then \(\alpha \leq \beta\).
\begin{code}      
      EQ -> case compare (list t) (list t') of
        GT -> LT -- If ~\(t \geq t'\)~ in the lexicographic order on lists
        LT -> GT -- then t majorizes t'.
        EQ -> EQ
\end{code}
Given a type \ctt{n} representing a natural number \(n\), we will enumerate the partitions of \(n\) recursively, using partitions of 1 as the base case.
\begin{code}
partitions :: (Natural n) => n -> [Partition n p]
partitions n = (fmap Partition . partitions' . value) n
--The function ~\ctt{value}~ produces the integer associated with the type-level
--natural number ~\ctt{n}~.
  where
  partitions' :: Int -> [[Int]]
  partitions' 1 = [[1]]
\end{code}
Let \(P_k\) denote the set of partitions of \(k\). For \(k \geq 2\), define \(f_k : P_k \rightarrow P_{k-1}\) to be the function that decrements the last integer in the partition by 1. Note that \(f_k\) is not injective. In particular, if \(\alpha = (a_1,\dots,a_{r-1},a_r)\) and \(\beta = (a_1,\dots,a_{r-1},a_r -1 ,1)\) are partitions of \(k\), then \(f_k(\alpha) = f_k(\beta)\). These are the only collisions, and we may generate \(P_k\) by computing the preimages of \(P_{k-1}\) under \(f\).
\begin{code}
  partitions' n = concatMap preimages (partitions' (n-1))
\end{code}
One of the preimages of ~\((a_1,\dots,a_r)\)~ under \rrh{\(f_k\)} will always be of the form ~\((a_1,\dots,a_r,1)\)~.
\begin{code}
  preimages part = [part ++ [1]] ++ bump part
\end{code}
There is only a second preimage if incrementing the last integer yields a partition. This is true in two cases. Either we have a single integer partition or the last integer is less than the one immediately before it.
\begin{code}
  bump part = case (reverse part) of
    y:[]       -> [[y+1]]    -- a one integer partition
    y1:y2:rest -> if y1 < y2 -- the last one is less
      then [reverse ((y1+1):y2:rest)]
      else []
\end{code}
We will denote the real span of \(\Irr(S_n)\) by \(CS_n\). Sorting the partitions of \(n\) using information in their type yields our chosen ordered basis for this set.
\begin{code}
irrBasis :: (Natural n, Natural p) => n -> p -> [Partition n p]
irrBasis n p = sort (partitions n)
\end{code}
Similarly, \(CS_n'\) will denote the real span of \(\IBr(S_n)\). Recall that \(\IBr(S_n)\) \rrh{is in} one-to-one correspondence with the set of \(p\)-regular partitions of \(n\). 
\begin{code}
ibrBasis :: (Natural n, Natural p) => n -> p -> [Partition n p]
ibrBasis n p = filter isRegular (irrBasis n p)
\end{code}
The \(p\)-regular partitions are the partitions that do not have the same integer appearing \(p\) or more times.
\begin{code}
isRegular :: (Natural p) => Partition n p -> Bool
isRegular t = (all (<p) . map length . group . list) t
  where p = (value . getP) t
\end{code}
To represent vectors in \(CS_n\) or \(CS_n'\) we use a list of rational numbers. The type \ctt{s} in \ctt{Vec s} is used to distinguish which vector space a particular vector belongs to. Vectors in \(CS_n\) will have type \ctt{VecCSn n p}, a synonym for \ctt{Vec (CSn n p)}. Similarly vectors in \(CS_n'\) will have type \ctt{VecCSn' n p}.
\begin{code}
type Scalar = Ratio Integer
newtype Vec s = Vec [Scalar] deriving (Show, Eq)
getSpace :: vec s -> s
getSpace = undefined
data CSn  n p = CSn
data CSn' n p = CSn'
type VecCSn  n p = Vec (CSn  n p)
type VecCSn' n p = Vec (CSn' n p)
\end{code}
We produce the vectors associated with basis elements by replacing every element of the basis with one or zero, as needed.
\begin{code}
embed :: (Natural n, Natural p) => Partition n p -> VecCSn n p
embed t = Vec (fmap (\x -> if x == t then 1 else 0) basis)
  where
  basis = irrBasis (getN t) (getP t)
embed' :: (Natural n, Natural p) => Partition n p -> VecCSn' n p
embed' t = if isRegular t
  then Vec (fmap (\x -> if x == t then 1 else 0) basis)
  else undefined
  where
  basis = ibrBasis (getN t) (getP t)
\end{code}
We are able to determine the dimension of a vector by using the type. This is done by computing the associated basis and counting the elements.
\begin{code}
class HasDimension a where --A type class allows us to define a function
  dimension :: a -> Int    --with a different body depending on the type.
instance (Natural n, Natural p) => HasDimension (CSn n p) where
  dimension x = length (irrBasis (getN x) (getP x))
instance (Natural n, Natural p) => HasDimension (CSn' n p) where
  dimension x = length (ibrBasis (getN x) (getP x))
instance HasDimension s => HasDimension (Vec s) where
  dimension = dimension . getSpace
\end{code}
In our implementation of vector arithmetic we define the oper\rrh{ations} only on vectors in the same space.  This constraint is encoded in the type \ctt{Vec s -> Vec s -> Vec s}. It is not possible to add vectors from different spaces without first performing an explicit conversion.
\begin{code}
(^+^) :: Vec s -> Vec s -> Vec s --Addition.
(Vec xs) ^+^ (Vec ys) = Vec (zipWith (+) xs ys)

(^-^) :: Vec s -> Vec s -> Vec s -- Subtraction.
v ^-^ u = v ^+^ (negateV u)

negateV :: Vec s -> Vec s -- Negation.
negateV (Vec xs) = Vec (fmap negate xs)

(*^) :: Scalar -> Vec s -> Vec s --Scalar multiplication.
c *^ (Vec xs) = Vec (fmap (*c) xs)

(^*^) :: Vec s -> Vec s -> Scalar --Scalar product.
(Vec xs) ^*^ (Vec ys) = sum (zipWith (*) xs ys)
\end{code}
We are ready to use Schur's theorem and Pate's theorem to generate our known inequalities and associated vectors of \(CS_n\). By Schur's theorem (thm \ref{thm:schur}), for every irreducible character \(\chi\) we have \(\chi \succeq 0\). The set \(\Irr(S_n)\) form our basis, so to list the associated vectors we just embed the basis into \(CS_n\).
\begin{code}
thmSchur :: (Natural n, Natural p) => n -> p -> [VecCSn n p]
thmSchur n p = fmap embed basis
  where
  basis = irrBasis n p
\end{code}
Now for Pate's theorem (thm \ref{thm:pate}). If we have a partition of \(n\) of the form \(\alpha =(a_1,\dots,a_i,\dots,a_k)\) and \(\beta = (a_1,\dots,a_i-1,\dots,a_k,1)\) is also a partition of \(n\) then \(\alpha \geq \beta\). Thus we have \(\beta(\e)\alpha - \alpha(\e)\beta \succeq 0\). To generate these vectors we \rrh{first} determine the character degrees using the hook length formula, then for each partition of \(n\) we enumerate all the ways we can generate another partition by ``moving a piece to the end.'' 
\begin{theorem}[Hook length formula {\cite[p.~56]{jamesandkerber}}]
\label{thm:hook}%
The degree of the irreducible character corresponding to the partition \(p\) of \(n\) is given by \[\frac{n!}{\prod_{(i,j) \in \lambda_p} h_{ij}}\] where \(\lambda_p\) is the set of cell coordinates for the Young diagram of \(p\) and \(h_{ij}\) is the ``hook length'' \rrh{of} the cell.
\end{theorem}
The hook length of a cell \((i,j)\) is the size of the set \[\{(x,y) | (x = i \,\wedge\, y \geq j) \vee (x \geq i \,\wedge\, y = j)\}.\] For example, consider the partition \([4,2^2,1]\). The cell \((2,1)\) has hook length 4 in the corresponding Young diagram.\\
\newcommand{\x}{\times}%
\newcommand{\h}{\hfill}%
\begin{center}\begin{tabular}{c@{\hskip 30pt}c}%
\young(\h\h\h\h,\x\x,\x\h,\x) & \young(7521,42,31,1) \\
Computing the hook length of \((2,1)\). & The hook length of every cell.\\
\end{tabular}\end{center}
Using the forumula, we have that the degree of \oc{4,2^2,1} is \(\frac{9!}{7\times5\times2\times1\times4\times2\times3\times1\times1} = 216\). We may translate the hook formula directly.
\begin{samepage}%
\begin{code}
xs @@ n = xs !! (n-1) --The list indexing operator ~\ctt{!!}~ uses indices that
                      --start with 0. Our operator ~\ctt{@@}~ will start at 1.
\end{code}
\end{samepage}%
\begin{code}
degree :: (Natural n, Natural p) => Partition n p -> Scalar
degree t = fromIntegral (fac n `div` product hooklens)
  where
  n = (value . getN) t
  fac x = (product . map toInteger) [1..x]
\end{code}
Next we produce a list of all of the cell coordinates in the tableau. The height of our Young tableau is the length of the corresponding partition and the width is the greatest (first) element of this partition. To produce a list of all cells in the Young tableau we simply enumerate \(\{(i,j) | 1 \leq i \leq h, 1 \leq j \leq w\}\) and filter out all the coordinates that refer to positions outside our tableau.
\begin{code}
  t' = list t
  h = length t'
  w = head t'
  coords = filter legal [(i,j) | i <- [1..h], j <- [1..w]]
\end{code}
Denote the current partition of interest by \(t = (t_1,\dots,t_h)\). To determine whether a pair of coordinates \((i,j)\) refers to a cell in the corresponding Young tableau, we check that the \rrh{column index} \(j\) is less than \(t_i\).
\begin{code}
  legal :: (Int,Int) -> Bool
  legal (i,j) = j <= (t' @@ i)
\end{code}
We compute \(h_{ij}\) numerically as follows. The number of cells to the right of \((i,j)\) is given by \(t_i - j\) and the number of cells below is \(\left|\{t_x | x > i, t_x \geq j\}\right|\).
\begin{code}
  hooklens = fmap hooklen coords
  hooklen :: (Int, Int) -> Integer
  hooklen pos = toInteger (1 + rightOf pos + below pos)
  rightOf (i,j) = (t' @@ i) - j
  below (i,j) = length [t' @@ x | x <- [(i+1)..h], (t' @@ x) >= j]
\end{code}
We enumerate the inequalities produced by Pate's theorem by, for each partition \(t\) of \(n\), producing the inequalities where \(t\) appears as the greater term.
\begin{code}
thmPate :: (Natural n, Natural p) => n -> p -> [VecCSn n p]
thmPate n p = concatMap thmPate1 basis
  where
  basis = irrBasis n p
\end{code}
To list the partitions less than \(t=(t_1,\dots,t_r)\), we loop through every \(i \in \{1,\dots,r\}\) and consider whether \(s_i=(t_1,\dots,t_i-1,\dots,t_r,1)\) is also a partition of \(n\). If so, Pate's theorem applies. We then know \(\frac{\oc{s}}{\oc{s}(\e)} \geq \frac{\oc{t_i}}{\oc{t_i}(\e)}\)  so we generate the vector \(\oc{s_i}(\e)\oc{t} - \oc{t}(\e)\oc{s}\). 
\begin{code}
thmPate1 :: (Natural n, Natural p) => Partition n p -> [VecCSn n p]
thmPate1 t = fmap mkVector [decrement i | i <- [1..r], verify i]
  where
  mkVector s = (degree s *^ embed t) ^-^ (degree t *^ embed s)
  t' = list t
  r = length t'
\end{code}
We need to verify that \(s_i=(t_1,\dots,t_i-1,\dots,t_r,1)\) is a partition. This is true when either \(t_i\) is the last term of \(t\) with \(t_i > 1\) or \(t_i\) occurs before the last term with \(t_i > t_{i+i}\).
\begin{code}
  verify :: Int -> Bool
  verify i | i == r && 
             (t' @@ i) > 1             = True
           | i < r &&
             (t' @@ i) > (t' @@ (i+1)) = True
           | otherwise                 = False
\end{code}
We construct the partition \(s_i\) by gluing the following sequences end to end. 
\begin{code}
  decrement :: Int -> Partition n p
  decrement i = Partition (
    (take (i-1) t')  ++ [(t' @@ i) - 1] ++ (drop i t') ++ [1] )
  {- ~%
\hspace{20pt}\((t_1,\dots,t_{i-1},\) %
\hspace{73pt}\(t_i -1,\)%
\hspace{73pt}\(t_{i+1},\dots,t_r,\)%
\hspace{40pt}\(1)\)~ -}
\end{code}
\begin{remark}
This does not pick up every inequality between irreducible characters generated by Pate's theorem. For example 
consider the partitions of \rrh{\(3\) \textup{:}} \(a =(3)\), \(b = (2,1)\), and \(c = (1^3)\). Direct 
application of Pate's theorem gives us \(a \geq b\) and \(b \geq c\), but \(a \geq c\) comes from transitivity. We 
are not generating the inequalities that come from transitivity, but this is not a problem because the vector 
\(a-c\) is in the convex cone of the vectors \(\{b - a, c-b\}\). Applying transitivity to extend our list of %
inequalities cannot create a vector outside the convex cone of the list we started with. %\abnote{Perhaps I should include a proof of this. The idea also applies elsewhere.}
\end{remark}
We have encoded all of the inequalities we will use to generate our convex cone as vectors in \(C S_n\), so now we 
just need to generate their images in \(C S_n'\) under the decomposition map. For this we use a type representing 
a linear map between two vector spaces. The type encodes the domain and codomain, but the internal representation is 
just a matrix of rational numbers. 
\begin{code}
data LinearMap d r = LinearMap [[Scalar]] deriving Show
domain :: LinearMap d r -> d
domain = undefined
range :: LinearMap d r -> r
range = undefined
matrix :: LinearMap d r -> [[Scalar]]
matrix (LinearMap m) = m
\end{code}
The decomposition matrix is then a value of this type.
\begin{code}
type DecompositionMatrix n p = LinearMap (CSn n p) (CSn' n p)
\end{code}
We implement the action of a linear map \(f\) through matrix multiplication.  
\begin{code}
apply :: (HasDimension d, HasDimension r) =>  LinearMap d r -> Vec d -> Vec r
apply f (Vec v) = Vec [entry i | i <- [1..h]]
  where
  mat = matrix f
  entry i = sum [((mat @@ i) @@ j) * (v @@ j)| j <- [1..w]]
  --If ~\(A \in \C^{h\times w}\)~, ~\(v \in \C^w\)~, and ~\(Av = w \in \C^h\)~, then ~\(w_i = \sum_{j=1}^w a_{ij}v_j\)~.
  w = (dimension . domain) f
  h = (dimension . range ) f
\end{code}
The full set of vectors used to generate our convex cone is then the decomposition matrix applied to the results of \ctt{thmSchur} and \ctt{thmPate}.
\begin{code}
cone :: (Natural n, Natural p) => DecompositionMatrix n p -> [VecCSn' n p]
cone d = fmap (apply d) (thmSchur n p ++ thmPate n p)
  where 
  n = (getN . domain) d
  p = (getP . domain) d
\end{code}
Our linear program will be of the form \(Ax = b\), where the columns of \(A\) are the vectors returned by \ctt{cone} and \(b\) is the vector representing the current Brauer character of interest. There is only one remaining obstacle to setting this up. Where are we going to get the decomposition matrix?
\input{../top_postamble}
