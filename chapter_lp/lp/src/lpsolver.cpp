//Linear program solver
// reads nonnegative linear program in MPS format from stdin 
// and prints results to stdout
//Original code: first_lp_from_mps.cpp
//Available at
//http://doc.cgal.org/latest/QP_solver/QP_solver_2first_lp_from_mps_8cpp-example.html
//
//Modified by Alexander Byaly.
#include <iostream>
#include <fstream>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact integral type

#include <CGAL/Gmpz.h>
typedef CGAL::Gmpz ET;
#define USED_TYPE "Gmpz"

//~ #include <CGAL/MP_Float.h>
//~ typedef CGAL::MP_Float ET;
//~ #define USED_TYPE "MP_Float"

//~ #include <CGAL/Gmpq.h>
//~ typedef CGAL::Gmpq ET;
//~ #define USED_TYPE "Gmpq"


// program and solution types
typedef CGAL::Quadratic_program_from_mps<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main() {
//  std::ifstream in ("first_nonnegative_lp.mps");
//  Program lp(in);         // read program from file
  Program lp(std::cin);         // read program from stdin
  assert (lp.is_valid()); // we should have a valid mps file,...
  assert (lp.is_linear());// ..and it encodes a linear program,...
  assert (lp.is_nonnegative()); // ...and it should be nonnegative

  // solve the program, using ET as the exact type
  Solution s = CGAL::solve_nonnegative_linear_program(lp, ET());

  // output solution
  std::cout << "Results of LP solver using " << USED_TYPE << ".\n";
  std::cout << s;
  if (s.is_infeasible())
  { int k = 0;
    for(Solution::Infeasibility_certificate_iterator i
                = s.infeasibility_certificate_begin(); 
        i != s.infeasibility_certificate_end();
        i++)
        { std::cout << k << ": " << *i <<"\n";
        }
  }
  return 0;
}
