#!/usr/bin/env runhaskell

import System.Process (readProcess)
import Data.Ratio
import Text.Read (readMaybe)
type Scalar = Ratio Integer

main = do 
  input <- getContents
  out <- readProcess "./lp/src/lpsolver" [] input
  let result = parselp out
  print result
  
parselp :: String -> Either [Ratio Integer] [Ratio Integer]
parselp s = case status of
                 "OPTIMAL"    -> Right $
                   parse_vector . tail 
                   . dropWhile (not . (=="variable values:")) $ l
                 "INFEASIBLE" -> Left $
                   parse_vector . tail $ l
                 _ -> error "no parse"
  where
  l = dropWhile (not . isPrefix "status:") . lines $ s
  status = (!!1) . words . head $ l
  isPrefix :: String -> String -> Bool
  isPrefix a b = a == take (length a) b

parse_vector :: [String] -> [Scalar]
parse_vector l = map (readFraction . (!!1) . words) l

readFraction :: String -> Rational
readFraction = r2 . map (\x -> if x == '/' then '%' else x)
    where
    r1 = readMaybe :: String -> Maybe Integer
    r2 :: String -> Rational
    r2 s = case r1 s of
            Just x -> toRational x
            Nothing -> read s
