{-# LANGUAGE FlexibleInstances #-}
module TableGen
where
import Appendix
import Solver
import Decomposition
import ConstructLP


import Control.Monad
import System.IO.Error
import Control.Exception

savepath n p k = concat ["./lp/",show n,"/",show p,"/",show k]

searchAll :: (Natural n, Natural p) => n -> p -> Int -> IO ()
searchAll n p i = do 
  statuses <- gen n p
  print statuses
  putStrLn $ concat [show . length $ statuses, " "
           , show . length . filter (==Dunno) $ statuses ]
  forM_ (zip [1..] statuses) $ \(k,s) ->
    case s of
      Dunno -> do 
        search n p k
        putStrLn (concat [show n," " ,show p," ",show k," complete."])
      _ -> return ()
  
readFileMaybe :: String -> IO (Maybe String)
readFileMaybe filepath = catch (fmap Just $ readFile filepath) handler
  where
  handler e = if isDoesNotExistError e 
              then return Nothing
              else ioError e
data Status = Yes | No | Dunno | Wat deriving (Show, Eq)
gen :: (Natural n, Natural p) => n -> p -> IO [Status]
gen n p = do
  d <- gap_decomposition_matrix n p
  let cols = cone d
  forM (zip [1..] bcs) $ \(k,t) -> do
    let file = savepath n p k ++ ".out"
    result <- (readFileMaybe file)
    let solution = (fmap read result) :: Maybe (Either [Scalar] [Scalar])
    return $ case solution of
      Nothing -> Dunno
      Just x -> if verify cols t x
                then case x of
                     Right _ -> Yes
                     Left  _ -> No
                else Wat
  where
  bcs = fmap embed' (ibrBasis n p)



ttt :: (Natural n, Natural p) =>
    n -> p -> (Int -> Bool) -> String
ttt n p b = concat $ ["\\begin{tabular}{|l|l|}\n\\hline"
  ,"\\(\\IBr(S_", show n,") \\quad p = ",show p,"\\) & LP Result\\\\"
  ,"\n\\hline"]  
  ++ fmap f (zip [1..] $ ibrBasis n p) 
  ++ ["\\hline\n\\end{tabular}"]
  where
  f (i,bc) = concat [tex . BC $ bc," & ", g i, "\\\\ \n"]
  g i = if b i then "Positive" else "Infeasible"

class PrintTex a where
  tex :: a -> String
  
instance PrintTex (Ratio Integer) where
  tex x = let (n,d) = (numerator x, denominator x) in
    case d of
    1 -> show n
    _ -> concat ["\frac{",show n,"}{",show d,"}"]

instance PrintTex (Partition a b) where
  tex (Partition l) = concatWith ',' . fmap (uncurry ex) . groupAndCount $ l
    where  
    groupAndCount xs = fmap (\l -> (head l, length l)) $ group xs
    ex i j = case j of 
      1 -> show i
      _ -> concat [ show i, "^{", show j, "}"]
    concatWith c [] = []
    concatWith c (x:xs) = foldl (\a b -> concat [a,[c],b]) x xs 
newtype BC a b = BC (Partition a b)
instance PrintTex (BC a b) where
  tex (BC p) = concat ["\\bc{", tex p,"}"]

