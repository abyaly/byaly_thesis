\input{../top_preamble}
\section{Linear programming appendix}
In this section we consolidate the external dependencies of our program and include the definitions that are tangentially related to our previous work.
\begin{code}
module Appendix 
        ( module Appendix
        , module Data.List
        , module Text.Read
        , module Data.Ratio
        , readProcess
        ) where
\end{code}
The source code and documentation for our external dependencies \rrh{are} available on Hackage \cite{hackage}.
\begin{code}
import Data.Ratio (Ratio, numerator, denominator)
import Data.List (sort, group, elemIndex, transpose, sortBy)
import Data.Ord (comparing)
import System.Process (readProcess, waitForProcess, runCommand)
import Text.Read (readMaybe)
\end{code}
The \ctt{sortOn} function sorts a list such that the images of the elements under the provided function \ctt{f} are in ascending order.
\begin{code}
sortOn :: (Ord b) => (a -> b) -> [a] -> [a]
sortOn f = fmap fst 
         . sortBy (comparing snd) 
         . fmap (\x -> (x, f x)) 
\end{code}
The types \ctt{One} and \ctt{S} define our type level natural numbers. For example, the number three is represented as \ctt{S (S One)}.
\begin{code}
data One = One
data S a = S a
\end{code}
In order to convert a type level natural to an integer we use the following \ctt{Natural} type class. A type class is needed here rather than an ordinary function because the domain is not represented by a single type. \ctt{One} and \ctt{S One} are different types.
\begin{code}
class (Show a) => Natural a where
  value :: (Num b) => a -> b
\end{code}
We define the \ctt{Natural} instances recursively, starting with \ctt{One}.
\begin{code}
instance Show One where
  show _ = "1"
instance Natural One where
  value _ = 1
instance (Natural n) => Show (S n) where
  show = show . value
instance (Natural n) => Natural (S n) where
  value = (+1) . value . (undefined :: S n -> n)
\end{code}
The instance of \ctt{Natural} for \ctt{S n} work as follows. The \ctt{value} of a variable \(x\) of type \ctt{S n} is defined to be 1 plus the \ctt{value} of a variable \(y\) of type \ctt{n}. Since it is possible to compute the \ctt{value} of \(y\) without knowing anything about it other than its type, the \ctt{undefined} does not get evaluated.

Next, we include abbreviations of some low order type level naturals for convenience.
\begin{code}
type N1 = One
type N2 = S N1
type N3 = S N2
type N4 = S N3
type N5 = S N4
type N6 = S N5
type N7 = S N6
type N8 = S N7
type N9 = S N8
type N10 = S N9
type N11 = S N10
type N12 = S N11
type N13 = S N12
type N14 = S N13
type N15 = S N14
type N16 = S N15
type N17 = S N16
type N18 = S N17
type N19 = S N18
type N20 = S N19
type N21 = S N20
type N22 = S N21
type N23 = S N22
type N24 = S N23
\end{code}
We include the following values to be used as arguments to functions that expect type level naturals.  These are used only to disambiguate type information and never get evaluated. They may be converted to integer values purely by inspecting their types, as shown in the definition of \ctt{value} above.
\begin{code}
n1 = undefined :: N1
n2 = undefined :: N2
n3 = undefined :: N3
n4 = undefined :: N4
n5 = undefined :: N5
n6 = undefined :: N6
n7 = undefined :: N7
n8 = undefined :: N8
n9 = undefined :: N9
n10 = undefined :: N10
n11 = undefined :: N11
n12 = undefined :: N12
n13 = undefined :: N13
n14 = undefined :: N14
n15 = undefined :: N15
n16 = undefined :: N16
n17 = undefined :: N17
n18 = undefined :: N18
n19 = undefined :: N19
n20 = undefined :: N20
n21 = undefined :: N21
n22 = undefined :: N22
n23 = undefined :: N23
n24 = undefined :: N24
\end{code}
\input{../top_postamble}
