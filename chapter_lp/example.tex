\input{../top_preamble}
\section{Two complete examples}
In this section we show the application of this technique from start to finish. First we consider the problem of proving Corollary \ref{corr:51}. We want to show that the function \bc{5,1} %
is in the convex cone of a set of vectors \(S\subseteq \C S_n\) satisfying \(d_v(A) \geq 0\) \rrh{for all \(v\in S\) and all \(A\in\pmat\)}. We populate \(S\) by applying the decomposition map to every vector of \(\Irr (S_6)\) and every inequality yielded by Pate's theorem (\ref{thm:pate}). The vectors of \(S\) then make up the columns of the matrix on the left hand side of Equation \ref{eq:s6p3a}.

{\tiny\begin{equation}\label{eq:s6p3a}
\left[\begin{array}{rrrrrrrrrrrrrrrrrrrrrrr}%
1 & 1 & 0 & 0 & 0 & 1 & 0 & 1 & 0 & 0 & 0 & 4 & 2 & -9 & 0 & -5 & 0 & 9 & 5 & 0 & 9 & 0 & 0 \\
0 & 1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & -1 & 1 & -9 & -9 & 11 & 1 & 9 & 5 & 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 16 & 10 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 & 0 & 1 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & -9 & 0 & 11 & 0 & 9 & 5 & -9 & 0 & -2 & -4 \\
0 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 0 & 0 & 0 & -1 & -9 & -9 & -5 & 0 & 9 & -3 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 & -9 & 0 & -5 & -1 & 9 & -3 & -9 & 9 & -1 & 1 \\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & -16 & 0 & 5 & -5 & 0 & 0
\end{array}\right] x =%
\left[\begin{array}{r}
0\\
1\\
0\\
0\\
0\\
0\\
0
\end{array}\right]
\end{equation}}%


The vector \bc{5,1}, when written in terms of our chosen basis is \(\langle 0,1,0,0,0,0,0 \rangle\). Thus the problem of writing \bc{5,1} as a nonnegative linear combination of vectors in \(S\) is the linear program \ref{eq:s6p3a}. Written out in MPS format, this linear program looks like so.
\begin{verbatim}
NAME          S6P3
ROWS
 E  R1
 E  R2
 E  R3
 E  R4
 E  R5
 E  R6
 E  R7
COLUMNS
    X1        R1                   1
    X2        R1                   1   R2                   1
    X3        R3                   1
    X4        R2                   1   R4                   1
    X5        R2                   1   R5                   1
    X6        R1                   1   R2                   1
    X6        R4                   1   R5                   1
    X6        R6                   1
    X7        R7                   1
    X8        R1                   1   R6                   1
    X9        R5                   1   R6                   1
    X10       R4                   1   R6                   1
    X11       R4                   1
    X12       R1                   4   R2                  -1
    X13       R1                  10   R2                   5
    X13       R5                  -5
    X14       R1                  -9   R2                  -9
    X14       R3                  16   R4                  -9
    X14       R5                  -9   R6                  -9
    X15       R2                  -9   R3                  10
    X15       R5                  -9
    X16       R1                  -5   R2                  11
    X16       R4                  11   R5                  -5
    X16       R6                  -5
    X17       R2                  10   R6                 -10
    X18       R1                   9   R2                   9
    X18       R4                   9   R5                   9
    X18       R6                   9   R7                 -16
    X19       R1                  10   R2                  10
    X19       R4                  10   R5                  -6
    X19       R6                  -6
    X20       R4                  -9   R6                  -9
    X20       R7                   5
    X21       R1                   9   R6                   9
    X21       R7                  -5
    X22       R4                 -10   R5                   5
    X22       R6                  -5
    X23       R4                  -4   R6                   1
RHS
    rhs       R2                   1
ENDATA
\end{verbatim}
Running this through a linear program solver produced the following solution vector.
\begin{verbatim}
[0,0,0,0,0,1/4,0,0,0,0,0,0,1/135,0,0,7/108,0,0,0,0,0,1/45,5/27]
\end{verbatim}
\begin{remark}
This solution is different from the vector used to prove corollary {\normalfont\ref{corr:51}}. \rrhnote{I changed the corollary number to normal font.} We do not generally expect the solutions to be unique, but the reason for the difference in this case is \rrh{that we} used vectors with integer coefficients to generate our convex cone.
\end{remark}
From the existence of this vector we may conclude that \(\bc{5,1} \succeq_{\pmat_6(3)} 0\)\rrh{, which in turn implies that \(\bc{5,1} \geq_\pmat \varepsilon\).}

Next we consider the group \(S_8\) and the prime \(p=5\). We will apply this technique to the Brauer character \bc{4^2}, which is the fifth element of \(\IBr(S_8)\) by the ordering we defined in Section \ref{section:constructlp}. The linear program for determining whether \bc{4^2} is in the convex cone generated by the Schur vectors and the Pate vectors is the following.\\

\bigskip

\scalemath{.33}{\left[\begin{array}{cccccccccccccccccccccccccccccccccccccccccccccccccccc}
1&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&7&0&0&0&0&0&70&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&1&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-1&21&0&0&70&64&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&64&21&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&1&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&70&64&0&0&0&0&0&0&0&0&70&56&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&1&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&70&0&0&0&-70&0&0&0&0&-42&0&0&0&64&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&1&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-7&-20&-20&0&-28&0&35&90&35&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&1&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-20&0&0&-28&0&0&90&35&0&0&70&90&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-28&0&-14&0&0&0&56&90&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&1&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&70&90&0&0&0&0&0&0&0&0&28&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&70&56&0&0&0&0&-70&0&-14&0&0&0&20&0&0&0\\
0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-21&0&-64&0&0&0&0&0&0&35&0&0&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-64&0&0&-70&0&-56&0&0&0&64&35&0&0&0&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-70&0&0&0&0&-42&0&-90&0&8&0&-70&0&0&20&21&0&0&0&0\\
0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-56&0&-42&0&0&0&0&0&28&64&0&0&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&28&0&0&0&0&0&0&-7\\
0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-35&0&-90&0&0&0&0&21&0&0&0&0&0&0\\
0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-90&0&-56&0&-70&0&-35&20&-43&0&0&7&0\\
0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-70&0&-14&0&0&0&20&-20&-21&1\\
0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&1&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&0&-64&0&-28&7&0&0\\
\end{array}\right]x =%
\left[\begin{array}{c}0\\0\\0\\0\\1\\0\\0\\0\\0\\0\\0\\0\\0\\0\\0\\0\\0\\0\\0\\\end{array}\right]}\\

\bigskip

After attempting to solve this linear program we produced the following Farkas certificate.\\
\begin{center}\scalemath{.33}{\left[\begin{array}{c}
329751752343552000000\\
0\\
299774320312320000000\\
419684048437248000000\\
-119909728124928000000\\
0\\
959277824999424000000\\
1049210121093120000000\\
104921012109312000000\\
209842024218624000000\\
0\\
1348984441405440000000\\
959277824999424000000\\
1049210121093120000000\\
0\\
0\\
0\\
0\\
0\\
\end{array}\right]}\end{center}

From this we know that \bc{4^2} is not in the convex cone. This does not, however, mean that there exists a \nice{p} matrix \(A\) such that \(\dbc{4^2}(A) < 0\), in other words, that \(\bc{4^2}\nsucceq_\pmat0\).  It just means that the inequalities due to Schur's theorem and Pate's theorem are not sufficient to guarantee the inequality \(\bc{4^2}\succeq_\pmat0\).  (If it should turn out that this inequality does not hold, this would provide a counterexample to Conjecture \ref{conj:schurforibr}, since if \(\bc{4^2}\nsucceq_\pmat0\) there exists a matrix \(A \in \pmat\) such that \(\dbc{4^2}(A) < 0 \leq \det(A)\).)
\input{../top_postamble}
